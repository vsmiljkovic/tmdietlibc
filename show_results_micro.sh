#!/bin/sh

# The script for preparing all results and drawing a graph
#   - input:  date_time of benchmarks running
#   - output: stamp-date_time.eps

# Run the script like e.g.:
#  . show_results.sh 2014-03-13-17-15

#benchs=("bayes" "genome" "intruder" "kmeans" "labyrinth" "ssca2" "vacation" "yada")
implementations=("nondet" "det") 
operations=("FGETC" "FGETS" "FREAD" "FPUTC" "FPUTS")

date_time=$1

implementations_str="${#implementations[@]}" # number of implementations

for im in "${implementations[@]}"
do 
   implementations_str="$implementations_str $im"
done

perf_files='' # input files for prepare_data.py script
micro_perf_file="micro_$date_time.perf" # the output of prepare_data.py and the input for gnuplot
micro_graph_file="micro_$date_time.eps" # the output of gnuplot

cd test/microbench/

for op in "${operations[@]}" 
do
   paste_cmd=''
   for im in "${implementations[@]}"
   do 
      paste_cmd="$paste_cmd time-$im/${date_time}_parsed_micro_${op}"
   done
   perf_file="micro_${date_time}_${op}.perf"
   perf_files="$perf_files $perf_file"
   paste_cmd="paste $paste_cmd | tail -n +2 > $perf_file"
   eval $paste_cmd
done

prepare_data_cmd="python prepare_data.py $implementations_str $perf_files > $micro_perf_file"
#echo "------- executing prepare_data.py script --------"
echo $prepare_data_cmd
eval $prepare_data_cmd
#echo "------- plotting create_graph.gp --------"
create_graph_cmd="gnuplot -e \"input_file='$micro_perf_file'\" -e \"output_file='$micro_graph_file'\" create_graph.gp"
echo $create_graph_cmd
eval $create_graph_cmd

cd ../..
