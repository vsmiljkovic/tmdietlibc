#include <stdio.h>
#include <libitm.h>
#include <pthread.h>
#include <detrans.h>
#include <dietlibc-tanger-stm.h>

int counter;
int tmp;
volatile int start;
double c, ccc;

void* thread_function1(void* arg) {
  start = 1;
  int i;
  ccc = 0;

  fprintf(stderr, "thread_func start\n");

  for (i = 0; i<100000; i++) {
//   __transaction_atomic {
     ccc += 0.12;
     ccc *= 0.1;
   }
//  }
  fprintf(stderr, "thread_func end ccc = %f\n", ccc);

  return NULL;
}

int main() {
  pthread_t t[2];
  counter = -2;
  c = 0.12;
  int i;

  start = 0;

  detrans_init();

  pthread_create(&t[0], NULL, thread_function1, NULL);
//  pthread_create(&t[1], NULL, thread_function2, NULL);


  for (i = 0; i < 10000; i++) {
    c += 0.31;
    c *= 1.7;
  }

  for (i = 0; i < 10000; i++) {
    TRANSACTION_BEGIN
      c += 0.31;
      c *= 1.7;
    TRANSACTION_END
  }

  while (!start) {}

  pthread_join(t[0], NULL);
//  pthread_join(t[1], NULL);
 
  printf("Program ends here, counter=%d, c = %f \n", counter, c);
  
  detrans_exit();
  return 0;
}

