include Makefile.common

.PHONY: all test check
.SILENT: check

all: $(LIBDIR)/$(DIETLIB)

test:
ifeq ($(ARCH),$(ARCH_I386))
	make -C ./test/RBTree
else
	make -C ./test-check
endif

check:
ifeq ($(ARCH),$(ARCH_I386))
	make check -C ./test/RBTree
else
	make check -C ./test-check
endif

clean: clean-lib 
clean-lib:
	$(RM) lib-x86_64/tmp/*
	$(RM) lib-x86_64/*.o
	$(RM) simple_txn simple_txn.o racey racey.o test/tiobench/*.o test/tiobench/tiotest
	$(RM) test/microbench/*.o test/microbench/micro_*

clean-test:
	make clean -C ./test/microbench
	make clean -C ./test/RBTree
	make clean -C ./test-check


# TM-dietlibc evaluation:
micro:
	make -C ./test/microbench $(MAKE_ARGS)
clean-micro:
	make clean -C ./test/microbench

redblack:
	make -C ./test/RBTree $(MAKE_ARGS)
clean-redblack:
	make clean -C ./test/RBTree


