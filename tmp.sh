unset ITM_STATISTICS
export MAKE_ARGS='SYNCHRO=USE_TM ARCH=x86_64'
export SYNCHRO=USE_TM
make ARCH=x86_64 SYNCHRO=USE_TM 
export MAKE_ARGS='ARCH=x86_64 FGETC=1 '
make clean-micro
make micro 
cp ./test/microbench/micro ./bench_exe/
mv ./bench_exe/micro ./bench_exe/micro_FGETC10-28_14_19_13_TM
echo ['micro_FGETC10-28_14_19_13_2000000_250']
echo []
echo []
echo [['./micro_FGETC10-28_14_19_13_TM 2000000 250', '']]
echo []
cd ./bench_exe/
if [ -a micro_FGETC10-28_14_19_13_2000000_250_all_output.txt ]; then rm micro_FGETC10-28_14_19_13_2000000_250_all_output.txt; fi
if [ -a micro_FGETC10-28_14_19_13_2000000_250_all_time.txt ]; then rm micro_FGETC10-28_14_19_13_2000000_250_all_time.txt; fi
if [ -a micro_FGETC10-28_14_19_13_2000000_250_results.txt ]; then rm micro_FGETC10-28_14_19_13_2000000_250_results.txt; fi
echo . TM TMIRR >> micro_FGETC10-28_14_19_13_2000000_250_results.txt
sync
./micro_FGETC10-28_14_19_13_TM 2000000 250 1  > tmp.txt
cat tmp.txt >> micro_FGETC10-28_14_19_13_2000000_250_all_output.txt
cat tmp.txt | grep Time >> tmp_time.txt
echo TM 1 >> micro_FGETC10-28_14_19_13_2000000_250_all_time.txt
cat tmp_time.txt >> micro_FGETC10-28_14_19_13_2000000_250_all_time.txt
cat tmp_time.txt | awk '{sum += $3;} END {print sum/NR;} ' >> tmp_results.txt
rm tmp_time.txt
echo 1 `cat tmp_results.txt `>> micro_FGETC10-28_14_19_13_2000000_250_results.txt
rm tmp_results.txt
sync
./micro_FGETC10-28_14_19_13_TM 2000000 250 2  > tmp.txt
cat tmp.txt >> micro_FGETC10-28_14_19_13_2000000_250_all_output.txt
cat tmp.txt | grep Time >> tmp_time.txt
echo TM 2 >> micro_FGETC10-28_14_19_13_2000000_250_all_time.txt
cat tmp_time.txt >> micro_FGETC10-28_14_19_13_2000000_250_all_time.txt
cat tmp_time.txt | awk '{sum += $3;} END {print sum/NR;} ' >> tmp_results.txt
rm tmp_time.txt
echo 2 `cat tmp_results.txt `>> micro_FGETC10-28_14_19_13_2000000_250_results.txt
rm tmp_results.txt
sync
./micro_FGETC10-28_14_19_13_TM 2000000 250 4  > tmp.txt
cat tmp.txt >> micro_FGETC10-28_14_19_13_2000000_250_all_output.txt
cat tmp.txt | grep Time >> tmp_time.txt
echo TM 4 >> micro_FGETC10-28_14_19_13_2000000_250_all_time.txt
cat tmp_time.txt >> micro_FGETC10-28_14_19_13_2000000_250_all_time.txt
cat tmp_time.txt | awk '{sum += $3;} END {print sum/NR;} ' >> tmp_results.txt
rm tmp_time.txt
echo 4 `cat tmp_results.txt `>> micro_FGETC10-28_14_19_13_2000000_250_results.txt
rm tmp_results.txt
sync
./micro_FGETC10-28_14_19_13_TM 2000000 250 8  > tmp.txt
cat tmp.txt >> micro_FGETC10-28_14_19_13_2000000_250_all_output.txt
cat tmp.txt | grep Time >> tmp_time.txt
echo TM 8 >> micro_FGETC10-28_14_19_13_2000000_250_all_time.txt
cat tmp_time.txt >> micro_FGETC10-28_14_19_13_2000000_250_all_time.txt
cat tmp_time.txt | awk '{sum += $3;} END {print sum/NR;} ' >> tmp_results.txt
rm tmp_time.txt
echo 8 `cat tmp_results.txt `>> micro_FGETC10-28_14_19_13_2000000_250_results.txt
rm tmp_results.txt
echo ' ' >> all_results_10-28_14_19_13.txt
echo 'micro_FGETC10-28_14_19_13_2000000_250 ' >> all_results_10-28_14_19_13.txt
cat micro_FGETC10-28_14_19_13_2000000_250_results.txt >> all_results_10-28_14_19_13.txt
rm -f tmp.txt
rm -f *_tmpstrace.txt
cd ../
