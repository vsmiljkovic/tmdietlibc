#include <sched.h>
#include <stdio.h>

void CPU_ZERO(cpu_set_t *set) {*set = 0;}
void CPU_SET(int cpu, cpu_set_t *set) {*set |= (1UL << cpu);}

#if 0
int __determine_cpumask_size (pid_t tid)
{
  INTERNAL_SYSCALL_DECL (err);
  int res;

  size_t psize = 128;
  void *p = alloca (psize);

  while (res = INTERNAL_SYSCALL (sched_getaffinity, err, 3, tid, psize, p),
         INTERNAL_SYSCALL_ERROR_P (res, err)
         && INTERNAL_SYSCALL_ERRNO (res, err) == EINVAL)
    p = extend_alloca (p, psize, 2 * psize);

  if (res == 0 || INTERNAL_SYSCALL_ERROR_P (res, err))
    return INTERNAL_SYSCALL_ERRNO (res, err);

  __kernel_cpumask_size = res;

  return 0;
}
#endif

void pin_cpu(int cpu)
{
    cpu_set_t set;

    CPU_ZERO(&set);
    CPU_SET(cpu, &set);
    if (sched_setaffinity(0, sizeof(set), &set) != 0) {
        printf("WARNING: Thread:%d could not be pinned!\n", cpu);
    }
/*    else {
       printf("NOTE: Thread:%d is pinned OK!\n", cpu);
    }*/
}

#if 0
int pthread_attr_setaffinity_np (pthread_attr_t *attr, size_t cpusetsize,
                                const cpu_set_t *cpuset)
{
  struct pthread_attr *iattr;

//  assert (sizeof (*attr) >= sizeof (struct pthread_attr));
  iattr = (struct pthread_attr *) attr;

  if (cpuset == NULL || cpusetsize == 0)
    {
      free (iattr->cpuset);
      iattr->cpuset = NULL;
      iattr->cpusetsize = 0;
    }
  else
    {
      if (__kernel_cpumask_size == 0)
        {
          int res = __determine_cpumask_size (THREAD_SELF->tid);
          if (res != 0)
            /* Some serious problem.  */
            return res;
        }

      /* Check whether the new bitmask has any bit set beyond the
         last one the kernel accepts.  */
      size_t cnt;
      for (cnt = __kernel_cpumask_size; cnt < cpusetsize; ++cnt)
        if (((char *) cpuset)[cnt] != '\0')
          /* Found a nonzero byte.  This means the user request cannot be
             fulfilled.  */
          return EINVAL;

      if (iattr->cpusetsize != cpusetsize)
        {
          void *newp = (cpu_set_t *) realloc (iattr->cpuset, cpusetsize);
          if (newp == NULL)
            return ENOMEM;

          iattr->cpuset = newp;
          iattr->cpusetsize = cpusetsize;
        }

      memcpy (iattr->cpuset, cpuset, cpusetsize);
    }

  return 0;
}
#endif
