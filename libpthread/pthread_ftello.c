#include "dietstdio.h"
#include <dietlibc-tanger-stm.h>

off_t ftello(FILE *stream) {
	off_t tmp;
	ATOMIC_BEGIN(&stream->m);
	tmp = ftello_unlocked(stream);
	ATOMIC_END(&stream->m);
	return tmp;
}
