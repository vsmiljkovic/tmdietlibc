#include "dietstdio.h"
#include "sys/mman.h"
#include <sys/shm.h>	/* for MEM_BLOCK_SIZE */
#include <unistd.h>
#include <dietlibc-tanger-stm.h>
#include <stdio.h>
#include <assert.h>
#include <malloc.h>

ssize_t __libc_read(int fd,void*buf,size_t count) __attribute__ ((transaction_pure));
ssize_t __libc_write(int fd,const void*buf,size_t count) __attribute__ ((transaction_pure));

static void lseek_undo(void *args) __attribute__ ((transaction_pure));
static void read_undo(void *args) __attribute__ ((transaction_pure));

struct _param {
 int fd;
 int pos;
};

typedef struct _param param;

param * add_params(int fd, int pos) __attribute__ ((transaction_pure));
param * add_params(int fd, int pos) {
  param * p = malloc(sizeof(param));
  assert(p && "could not allocate param");
  p->fd = fd;
  p->pos = pos;
  return p;
}

void free_params(param * p) __attribute__ ((transaction_pure));
void free_params(param * p) {
    free(p);
}

/**
 * Tx of lseek sys call and undo function for it.
**/
void lseek_undo(void *args) {
  // the previous value of the file pointer was already restored by the TM system
  lseek(((param*)args)->fd, ((param*)args)->pos, SEEK_SET);
  free(args);
}

off_t tx_lseek(int fildes, off_t offset, int whence, int* filpos) {
  off_t tmp;
  param * p;
#if defined USE_TM || defined USE_TMIRR
//  SET_SYSCALLSAFE;
#ifdef DETRANS
  SET_IRREVOCABILITY;
#endif
#endif
#ifndef DETRANS
  p = add_params(fildes, *filpos);
#endif
  (*filpos)=0; // causing possible conflict before syscall
  tmp = lseek(fildes,offset,whence);

#ifndef DETRANS
  REGISTER_UNDO((void (*)(void*))lseek_undo, p);
  REGISTER_DEFERRAL((void (*)(void*))free_params, p);
#endif
  (*filpos) = tmp; // the real value of the file pointer after the syscall
  return tmp;
}

/**
 * Tx of __libc_read sys call and undo function for it.
**/
void read_undo(void *args) {
  lseek_undo(args);
  free(args);
}

ssize_t tx_read(int fildes, void*buf, size_t len, int* filpos) {
  ssize_t tmp;
  param * p;
#if defined USE_TM || defined USE_TMIRR
//  SET_SYSCALLSAFE;
#ifdef DETRANS
  SET_IRREVOCABILITY;
#endif
#endif
  
#ifndef DETRANS
  p = add_params(fildes, *filpos);
#endif
  (*filpos)++; // causing possible conflict before syscall
  tmp = __libc_read(fildes,buf,len);
  (*filpos) += tmp-1; // the real value of the file pointer after the syscall
			       // (-1 because of the previous incrementation)
#ifndef DETRANS
  REGISTER_UNDO((void (*)(void*))read_undo, p);
  REGISTER_DEFERRAL((void (*)(void*))free_params, p);
#endif
  return tmp;
}

/**
 * Tx of write sys call.
**/
ssize_t tx_write(int fildes,const void*buf,size_t count, int* filpos) {
  size_t ret;
#if defined USE_TM || defined USE_TMIRR
//  SET_SYSCALLSAFE;
  SET_IRREVOCABILITY;
#endif
  (*filpos)++;
  ret = __libc_write(fildes, buf, count);
  (*filpos) += ret-1; // the real value of the file pointer after the syscall
				// (-1 because of the previous incrementation)
  return ret;
}
