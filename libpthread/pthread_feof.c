#include "dietstdio.h"
#include <dietlibc-tanger-stm.h>

int feof(FILE*stream) {
	int tmp;
 	pthread_mutex_lock(&stream->m);
	tmp = feof_unlocked(stream);
	pthread_mutex_unlock(&stream->m);
	return tmp;
}
