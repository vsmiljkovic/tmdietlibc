#include "dietstdio.h"
#include <dietlibc-tanger-stm.h>

int fseek(FILE *stream, long offset, int whence) {
  int tmp;
  ATOMIC_BEGIN(&stream->m);
  tmp = fseek_unlocked(stream, offset, whence);
  ATOMIC_END(&stream->m);
  return tmp;
}

int _seek(FILE *stream, long offset, int whence) {
  int tmp;
  ATOMIC_BEGIN(&stream->m);
  tmp = seek_unlocked(stream, offset, whence);
  ATOMIC_END(&stream->m);
  return tmp;
}
