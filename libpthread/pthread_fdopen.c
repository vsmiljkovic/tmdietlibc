#include "dietstdio.h"
#include <dietlibc-tanger-stm.h>

FILE* fdopen(int filedes, const char *mode) {
	FILE *tmp;
        pthread_mutex_lock(NULL);
	tmp = fdopen_unlocked(filedes, mode);
	pthread_mutex_unlock(NULL);
	return tmp;
}
