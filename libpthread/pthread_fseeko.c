#include "dietstdio.h"
#include <dietlibc-tanger-stm.h>

int fseeko(FILE *stream, off_t offset, int whence) {
	int tmp;
	ATOMIC_BEGIN(&stream->m);
	tmp = fseeko_unlocked(stream, offset, whence);
	ATOMIC_END(&stream->m);
	return tmp;
}
