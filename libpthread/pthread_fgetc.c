#include "dietstdio.h"
#include <dietlibc-tanger-stm.h>

int fgetc(FILE *stream) {
  int tmp;
  ATOMIC_BEGIN(&stream->m);
  tmp=fgetc_unlocked(stream);
  ATOMIC_END(&stream->m);
  return tmp;
}
