#include "dietstdio.h"
#include <dietlibc-tanger-stm.h>

FILE *freopen(const char *path, const char *mode, FILE *stream) {
	FILE *tmp;
	pthread_mutex_lock(NULL);
	tmp = freopen_unlocked(path, mode, stream);
	pthread_mutex_unlock(NULL);
	return tmp;
}
