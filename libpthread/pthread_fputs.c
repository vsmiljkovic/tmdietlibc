#include <dietstdio.h>
#include <dietlibc-tanger-stm.h>

int fputs(const char*s, FILE *stream) {
  int tmp;
  ATOMIC_BEGIN(&stream->m);
  tmp=fputs_unlocked(s,stream);
  ATOMIC_END(&stream->m);
  return tmp;
}

