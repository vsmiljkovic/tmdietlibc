#include "dietstdio.h"
#include <dietlibc-tanger-stm.h>

int ungetc(int c, FILE *stream) {
  int tmp;
  ATOMIC_BEGIN(&stream->m);
  tmp = ungetc_unlocked(c, stream);
  ATOMIC_END(&stream->m);
  return tmp;
}
