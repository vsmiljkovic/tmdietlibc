#include "dietstdio.h"
#include <dietlibc-tanger-stm.h>

long ftell(FILE *stream) {
	long tmp;
	ATOMIC_BEGIN(&stream->m);
	tmp = ftell_unlocked(stream);
	ATOMIC_END(&stream->m);
	return tmp;
}

