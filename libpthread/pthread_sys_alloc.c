#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "thread_internal.h"

#include <dietlibc-tanger-stm.h>

static pthread_mutex_t mutex_alloc=PTHREAD_MUTEX_INITIALIZER;

void free(void *ptr) {
  _pthread_descr this=__thread_self();
  __NO_ASYNC_CANCEL_BEGIN_(this);
  __pthread_mutex_lock(&mutex_alloc,this);
  __libc_free(ptr);
  __pthread_mutex_unlock(&mutex_alloc,this);
  __NO_ASYNC_CANCEL_END_(this);
}

void *malloc(size_t size) {
  register void *ret;
    _pthread_descr this=__thread_self();
    __NO_ASYNC_CANCEL_BEGIN_(this);
    __pthread_mutex_lock(&mutex_alloc,this);
    ret=__libc_malloc(size);
    __pthread_mutex_unlock(&mutex_alloc,this);
    __NO_ASYNC_CANCEL_END_(this);
  return ret;

}

void *realloc(void* ptr, size_t size) {
  register void *ret;
  _pthread_descr this=__thread_self();
  __NO_ASYNC_CANCEL_BEGIN_(this);
  __pthread_mutex_lock(&mutex_alloc,this);
  ret=__libc_realloc(ptr, size);
  __pthread_mutex_unlock(&mutex_alloc,this);
  __NO_ASYNC_CANCEL_END_(this);
  return ret;
}

void *calloc(size_t nmemb, size_t _size){
  return __libc_calloc(nmemb, _size);
}


size_t malloc_usable_size(void *__ptr) {
  return libc_malloc_usable_size(__ptr);
}

int posix_memalign(void **__memptr, size_t __alignment, size_t __size) {
  return libc_posix_memalign(__memptr, __alignment, __size);
}



