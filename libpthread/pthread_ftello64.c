#include "dietstdio.h"
#include <stdio.h>
#include <dietlibc-tanger-stm.h>

#if __WORDSIZE == 32

off64_t ftello64(FILE *stream) {
	off64_t tmp;
	ATOMIC_BEGIN(&stream->m);
	tmp = ftello64_unlocked(stream);
	ATOMIC_END(&stream->m);
	return tmp;
}

#endif
