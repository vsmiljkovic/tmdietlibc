#include "dietwarning.h"

#ifndef WANT_THREAD_SAFE

int errno;

link_warning("errno","\e[1;33;41m>>> your multithreaded code uses errno! <<<\e[0m");

#endif
