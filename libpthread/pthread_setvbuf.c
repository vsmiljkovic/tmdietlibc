#include "dietstdio.h"
#include <dietlibc-tanger-stm.h>

static char* tanger_wrapperpure_setvbufunlocked(FILE *stream, char *buf, int mode, size_t size) __attribute__ ((weakref("setvbuf_unlocked")));

/*
 * Lock version of setvbuf.
**/
int setvbuf(FILE *stream, char *buf, int mode, size_t size) {
  int tmp;
  pthread_mutex_lock(&stream->m);
  tmp = setvbuf_unlocked(stream, buf, mode, size);
  pthread_mutex_unlock(&stream->m);
  return tmp;
}

