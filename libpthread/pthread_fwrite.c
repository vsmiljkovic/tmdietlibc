#include "dietstdio.h"
#include <dietlibc-tanger-stm.h>

/*
 * Lock version of fwrite.
**/

size_t fwrite_lock(const void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t tmp;
	pthread_mutex_lock(&stream->m);
	tmp = fwrite_unlocked_lock(ptr, size, nmemb, stream);
	pthread_mutex_unlock(&stream->m);
	return tmp;
}

size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t tmp;
	ATOMIC_BEGIN(&stream->m);
	tmp = fwrite_unlocked(ptr, size, nmemb, stream);
	ATOMIC_END(&stream->m);
	return tmp;	
}
