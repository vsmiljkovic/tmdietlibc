#include "dietstdio.h"
#include <stdio.h>
#include <dietlibc-tanger-stm.h>

#if __WORDSIZE == 32

int fseeko64(FILE *stream, off64_t offset, int whence) {
	int tmp;
	ATOMIC_BEGIN(&stream->m);
	tmp = fseeko64_unlocked(stream, offset, whence);
	ATOMIC_END(&stream->m);
	return tmp;
}
#endif
