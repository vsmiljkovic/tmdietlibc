#include "dietstdio.h"
#include <dietlibc-tanger-stm.h>

char* fgets(char *s, int size, FILE *stream) {
  char* tmp;
  ATOMIC_BEGIN(&stream->m);
  tmp = fgets_unlocked(s,size,stream);
  ATOMIC_END(&stream->m);
  return tmp;
}
