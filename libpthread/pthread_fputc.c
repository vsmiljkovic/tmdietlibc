#include <dietstdio.h>
#include <dietlibc-tanger-stm.h>

int fputc(int c, FILE *stream) {
  int tmp;
  ATOMIC_BEGIN(&stream->m);
#if defined USE_TM || defined USE_TMIRR
  tmp=tx_fputc_unlocked(c,stream);
#elif defined USE_LOCK
  tmp=fputc_unlocked(c,stream);
#else
#warning "Synchronization mechanism not set"
#endif
  ATOMIC_END(&stream->m);
  return tmp;
}

int fputc_lock(int c, FILE *stream) {
  int tmp;
  pthread_mutex_lock(&stream->m);
  tmp=fputc_unlocked(c,stream);
  pthread_mutex_unlock(&stream->m);
  return tmp;
}
