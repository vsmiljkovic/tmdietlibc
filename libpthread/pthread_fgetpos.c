#include "dietstdio.h"
#include <dietlibc-tanger-stm.h>
#include <unistd.h>

int fgetpos(FILE *stream, fpos_t *pos) {
	int tmp;
	ATOMIC_BEGIN(&stream->m);
	tmp = fgetpos_unlocked(stream, pos);
	ATOMIC_END(&stream->m);
	return tmp;
}

