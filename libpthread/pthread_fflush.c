#include <dietstdio.h>
#include <dietlibc-tanger-stm.h>

int fflush(FILE *stream) {
  int tmp;
#if defined USE_TM || defined USE_TMIRR
  TRANSACTION_BEGIN
  tmp=tx_fflush_unlocked(stream);
  TRANSACTION_END
#elif defined USE_LOCK
  if (stream) pthread_mutex_lock(&stream->m);
  tmp=fflush_unlocked(stream);
  if (stream) pthread_mutex_unlock(&stream->m);
#else
#warning "Synchronization mechanism not set"
#endif
  return tmp;
}

int fflush_lock(FILE *stream) {
  int tmp;
  if (stream) pthread_mutex_lock(&stream->m);
  tmp=fflush_unlocked(stream);
  if (stream) pthread_mutex_unlock(&stream->m);
  return tmp;
}
