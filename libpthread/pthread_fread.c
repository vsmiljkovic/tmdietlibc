#include "dietstdio.h"
#include <dietlibc-tanger-stm.h>

size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream) {
  size_t tmp;
  ATOMIC_BEGIN(&stream->m);
  tmp = fread_unlocked(ptr, size, nmemb, stream);
  ATOMIC_END(&stream->m);
  return tmp;
}
