#include "dietstdio.h"
#include <dietlibc-tanger-stm.h>

FILE *fopen(const char *path, const char *mode) {
	FILE *tmp;
	tmp = fopen_unlocked(path, mode);
	return tmp;
}

FILE *_open(const char *path, int flags, int m) {
	FILE *tmp;
	tmp = open_unlocked(path, flags, m);
	return tmp;
}
