#include "dietstdio.h"
#include <dietlibc-tanger-stm.h>


void clearerr(FILE *stream) {
	pthread_mutex_lock(&stream->m);
	clearerr_unlocked(stream);
        pthread_mutex_unlock(&stream->m);
}
