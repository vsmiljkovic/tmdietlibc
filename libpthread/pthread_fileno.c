#include "dietstdio.h"
#include <dietlibc-tanger-stm.h>


int fileno(FILE*stream) {
	int tmp;
	ATOMIC_BEGIN(&stream->m);
	tmp = fileno_unlocked(stream);
	ATOMIC_END(&stream->m);	
	return tmp;
}
