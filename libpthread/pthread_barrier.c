#include <pthread.h>
#include <detrans.h>

/* TM-changes - functions for working with barriers needed by RMS-TM/ScalParC */
void pthread_barrier_init(pthread_barrier_t* b, void* s, int n) 
{
    pre_pthread_barrier_init(b, n);
    pthread_cond_init(&b->complete, NULL);
    pthread_mutex_init(&b->mutex, NULL);
    b->count = n;
    b->crossing = 0;
}

void pthread_barrier_wait(pthread_barrier_t* b)
{
    pre_pthread_barrier_wait(b);
   
     pthread_mutex_lock(&b->mutex);
    /* One more thread through */
    b->crossing++;
    /* If not all here, wait */
    if (b->crossing < b->count) {
        pthread_cond_wait(&b->complete, &b->mutex);
    } else {
        pthread_cond_broadcast(&b->complete);
        /* Reset for next time */
        b->crossing = 0;
    }
    pthread_mutex_unlock(&b->mutex);

    post_pthread_barrier_wait();
}
/* TM-changes end */
