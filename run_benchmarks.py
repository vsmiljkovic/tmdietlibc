#!/usr/local/bin/python

from __future__ import with_statement

import datetime, time, os.path, sys

datetime = datetime.datetime.now().strftime("%m-%d_%H_%M_%S")

bench_run_dir = "./bench_exe/"

ids = []
tx_id = None


  #  Options for compiling and running benchmarks: [ a1, a2, a3, a4, a5]
  #   a1 = target in the Makefile;   a2 = location of the executable
  #   a3 = compilation options;      a4 = default name of exe
  #   a7 = II running options (after #threads)
microbench_options = [
  ['micro', './test/microbench/', 'FGETC=1',  'micro', 'micro_FGETC',  '2000000 250', ''],
#  ['micro', './test/microbench/', 'FREAD=1',  'micro', 'micro_FREAD',  '2000000 250 1', ''],
#  ['micro', './test/microbench/', 'FGETS=1',  'micro', 'micro_FGETS',  '2000000 250 2', ''],
#  ['micro', './test/microbench/', 'FPUTC=1',  'micro', 'micro_FPUTC',  '2000000 250', ''],
#  ['micro', './test/microbench/', 'FPUTS=1',  'micro', 'micro_FPUTS',  '2000000 250 2', ''],
]

redblackbench_options = [
  ['redblack', 'test/RBTree/', 'SYSLIB=DIETLIBC', 'set_harness', 'redblack', '16 64 100000', ''],
  ['redblack', 'test/RBTree/', 'SYSLIB=DIETLIBC', 'set_harness', 'redblack', '16 128 100000', ''],
  ['redblack', 'test/RBTree/', 'SYSLIB=DIETLIBC', 'set_harness', 'redblack', '16 192 100000', ''],
]

bench_options_x86_64 = microbench_options
bench_options_i386 = redblackbench_options

do_stats = 0 # = 1, Statistics On. = 0, Statistics Off.

if do_stats:
  print "export ITM_STATISTICS=-" # necessary for TinySTM
  stats = "STATS=1" # necessary for TM-dietlibc
  debug = "DEBUG=1"
else:
  print "unset ITM_STATISTICS" # necessary for TinySTM
  stats = ""
  debug = ""

do_test = 0 # = 1 , Check compilation and run. = 0, Compile and run normally.
if do_test:
  num_threads = ['1', '2', '4', '8']
  num_it = 1;
  synchros = ['TM']
  strace = 0;
else:
  num_threads = ['1', '2', '4', '8']
  num_it = 1;
  synchros = ['TM']
  strace = 0
do_benchrun = 1 # = 1 , run all compiled benchmarks. = 0 , don't run benchmarks
do_simulate = 0 # = 1 , copy the binary for the sumulation. = 0 , don't prepare simulation
#simulator1 =  "micro:ptlsim.xen.LLB_256"
simulator1 = "micro:ptlsim.xen.LLB_8.L1_RS"
simulator2 = "micro:ptlsim.xen.LLB_256.L1_RS"
simulator_dir = "/home/tm/cluster/exp/"

# Hash table with arrays:
#  bench_runs['NAME']   # bench name + options (will be used for name of the file with results
#  bench_runs['LOCK']   # command for running LOCK bench
#  bench_runs['TM']     # command for running TM bench
#  bench_runs['TMIRR']  # command for running TMIRR bench

bench_runs = {}
bench_runs['NAME'] = []
bench_runs['LOCK'] = []
bench_runs['LOCKORIG'] = []
bench_runs['TM'] = []
bench_runs['TMIRR'] = []

if do_test:
  print "echo '----------------------------------------------------------'"
  print "echo '------------------------ TESTING -------------------------'"
  print "echo '----------------------------------------------------------'"

# Create a directory for bench executables
# and create a file with name file.txt and size of ~ 2.5GB
if not os.path.exists(bench_run_dir):
  print "mkdir", bench_run_dir
if do_benchrun:
  if not os.path.isfile(bench_run_dir+"file.txt"):
    print "cd", bench_run_dir
    print "echo -n 'Although the number of TM implementations and TM-based applications grows, there is no standard system library that a wide range of developers can adopt. ' > file.txt"
    print "for i in {1..24}; do head file.txt > file2.txt && head file.txt >> file2.txt && mv file2.txt file.txt; done"
    print "cd ../"


#for arch in ['x86_64', 'i386']:
for arch in ['x86_64']:
#for arch in ['i386']:
  for synch in synchros:
    # make the benchmark
    if arch == 'x86_64':
      bench_options = bench_options_x86_64
    else:
      bench_options = bench_options_i386

    print "export", "MAKE_ARGS='SYNCHRO=USE_" + synch, "ARCH=" + arch + "'"
    print "export", "SYNCHRO=USE_"+synch
    if synch != 'LOCKORIG':
      # make the library
#      print "make clean"
      print "make", "ARCH=" + arch, "SYNCHRO=USE_"+synch, debug

    for bench, loc, arg, exe, newname, run_opt, run_opt2 in bench_options:
      print "export", "MAKE_ARGS='ARCH=" + arch, arg, stats + "'"
      
      if bench == 'rmstm':
        name = newname+datetime
      else:
        name = newname+datetime+"_"+run_opt

      name = name.replace(' ', '_')
      if not name in bench_runs['NAME']:
        bench_runs['NAME'].append(name)
   
      print "make", "clean-" + bench
      print "make", bench, debug

      print "cp", loc+exe, bench_run_dir
      print "mv", bench_run_dir + exe, bench_run_dir + newname + datetime + "_" + synch

      if do_simulate:
        print "sudo cp", bench_run_dir + newname + datetime + "_" + synch, simulator_dir+"micro/binary/"

      bench_runs[synch].append(["./" + newname + datetime + "_" + synch + " " + run_opt, run_opt2])



print "echo", bench_runs['NAME']
print "echo", bench_runs['LOCK']
print "echo", bench_runs['LOCKORIG']
print "echo", bench_runs['TM']
print "echo", bench_runs['TMIRR']


i = 0

# Execute benchmarks, save results and create tables with results.
#  tmp.txt :           tmp file for a bench output
#  tmp_time.txt :      tmp file for a time from the bench output
#  tmp_results.txt :   tmp file for all times from the bench output with the same #threads
# filename_all_output: all outputs from the bench
# filename_all_time :  all time for the bench
# filename_results :   results in a table

if do_benchrun:
  print "cd", bench_run_dir
  for filename in bench_runs['NAME']:
    print "if [ -a", filename+"_all_output.txt ]; then rm", filename+"_all_output.txt; fi"
    print "if [ -a", filename+"_all_time.txt ]; then rm", filename+"_all_time.txt; fi"
    print "if [ -a", filename+"_results.txt ]; then rm", filename+"_results.txt; fi"

    print "echo . TM TMIRR >>", filename+"_results.txt"
#    if strace == 1:
#      print "echo . TM TMIRR >>", filename+"_strace.txt"
    for th in num_threads:
      for synch in synchros:
        for it in range(num_it):
          print "sync"
#          print "setarch x86_64 -RL", bench_runs[synch][i][0], th, bench_runs[synch][i][1], "> tmp.txt"
          print bench_runs[synch][i][0], th, bench_runs[synch][i][1], "> tmp.txt"
          print "cat tmp.txt >>", filename+"_all_output.txt"
          print "cat tmp.txt | grep Time >> tmp_time.txt"
#          print "cat tmp.txt | grep '#txs        :' >> tmp_time.txt"

        if strace == 1:
          print "strace -c -f -o", filename+"_tmpstrace.txt" , bench_runs[synch][i][0], th, bench_runs[synch][i][1]
          print "echo", synch, th, ">>", filename+"_strace.txt"
          print "cat", filename+"_tmpstrace.txt >>", filename+"_strace.txt"
        print "echo", synch, th, ">>", filename+"_all_time.txt"
        print "cat tmp_time.txt >>", filename+"_all_time.txt"
        print "cat tmp_time.txt | awk '{sum += $3;} END {print sum/NR;} ' >> tmp_results.txt"
#        print "cat tmp_time.txt | awk '{sum += $5;} END {print sum/NR;} ' >> tmp_results.txt"
#	print "cat tmp_time.txt | awk '{sum += $5;} END {print sum/NR;} '"
        
        print "rm tmp_time.txt"
      print "echo", th, "`cat tmp_results.txt `>>", filename+"_results.txt"
      print "rm tmp_results.txt"

    i = i + 1
     
    print "echo ' ' >> all_results_"+datetime+".txt"
    print "echo '" + filename, "' >> all_results_"+datetime+".txt"
    print "cat", filename+"_results.txt >> all_results_"+datetime+".txt"

  print "rm -f tmp.txt"
  print "rm -f *_tmpstrace.txt"

 
  print "cd ../"
   

if do_simulate:
  with open("cmd-micro-"+datetime, "w") as f:
    for th in num_threads:
      for filename in bench_runs['NAME']:
        for synch in synchros:
          for it in range(num_it):
            f.write("%s:TINYSTM='-fileout mongo.db' %s %s\n" % (simulator2, bench_runs[synch][i][0], th))
        i = i + 1
      i = 0

  print "sudo mv cmd-micro-" + datetime, simulator_dir





