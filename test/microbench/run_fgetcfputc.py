#!/usr/local/bin/python
file_size = 7996
num_chars = ['2049']
['1', '2', '3', '5', '8', '17', '32']#, '513', '1025', '1026', '2049']

print "cp fileDIET.txt fileGLIBC.txt"

# compile with dietlibc
print "make SYNCHRO=USE_TM DIETLIBC=1" 
print "mv microGETPUT microDIET"

#compile with glibc 
print "gcc microGETPUT.c -o microGLIBC" 

print "rm -f diet.txt glibc.txt diff.txt"


for num_ch in num_chars:
  print "./microDIET", num_ch, file_size, "> diet.txt"
  print "./microGLIBC", num_ch, file_size, "> glibc.txt"
  print "diff diet.txt glibc.txt > diff1.txt"
  print "cat diff1.txt"
  print "echo 'number_of_characters = '", num_ch, ">> diff.txt"
  print "cat diff1.txt >> diff.txt"

  # testing changes of fgetc+fputc
  print "diff fileGLIBC.txt fileDIET.txt >> diff.txt"
  print "cat diff.txt"
