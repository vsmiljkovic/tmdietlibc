#!/usr/local/bin/python
file_size = 8354 # size of the iput file fileABC.txt
num_chars = ['2', '3', '5', '8', '17', '32', '513', '1025', '1026', '2049']

# compile with dietlibc
print "make SINGLE=1 SYNCHRO=USE_LOCK" 
print "mv microFGETCFPUTC microDIET"

#compile with glibc 
print "gcc microFGETCFPUTC.c -o microGLIBC" 

print "rm -f diet.txt glibc.txt"

for num_ch in num_chars:
    print "./microDIET", num_ch, file_size, ">> diet.txt"
    print "./microGLIBC", num_ch, file_size, ">> glibc.txt"

print "diff diet.txt glibc.txt > diff.txt"
print "cat diff.txt"
