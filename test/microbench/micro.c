/*
 *   Testing: fgets function with num_chars read characters. 
 */

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sched.h>
#include <dietstdio.h>
#ifndef USE_LOCK
#include <dietlibc-tanger-stm.h>
#endif

#include <detrans.h>

#define MB 1048576
#define USECSPERSEC 1000000

#define PIN_THREADS 1
#define USE_PADDING 1
#define USE_NOTHING 0

typedef struct thread_data {
#if USE_PADDING == 1
   char pad1[64];
#endif
   FILE* file;
   unsigned int seed;
   unsigned int num_of_char_read;
   char *s;
   int tid;
   int last_char;
   char* last_string;
   int last_fseek;
   int error_flag;
   int nb_chars;
   int nb_it;
   int nb_rand;
#if USE_PADDING == 1
   char pad2[64];
#endif
}thread_data_t;
pthread_barrier_t barr;
pthread_t *threads;
FILE* file;
char filePath[] = "file.txt";
long offset_coeff = 100 * MB;

int get_rand_offset(unsigned int* seed) __attribute__ ((transaction_pure));
int get_rand_offset(unsigned int* seed) {
   int result;
   result = rand_r(seed);
   if (result > offset_coeff) {
      result = result % offset_coeff;
   }
   return result;
}


// Worker thread function
void *thread_function(void *data){
   int i;
   volatile int w, we;
   thread_data_t *d = (thread_data_t *)data;
   char* swrite = d->s;

#if PIN_THREADS == 1
   pin_cpu(d->tid);
#endif

   int c = 'x';
   char* sread = (char*)malloc((d->nb_chars+1)*sizeof(char));

   pthread_barrier_wait(&barr);

   for (i=0; i<d->nb_it; i++) {
#if USE_NOTHING != 1
#ifdef FSEEK
      int offset = get_rand_offset(&(d->seed));
      if (d->last_fseek = fseek(file, offset, SEEK_SET)) {
         d->error_flag = 1;
      }
#endif
#ifdef FGETC
      if ((d->last_char = fgetc(file)) == EOF) {
         printf("fgetc error\n");
         d->error_flag = 1;
      }
#endif
#ifdef FPUTC
      if (fputc(c, file) == EOF) {
         printf("fputc error\n");
         d->error_flag = 1;
      }
#endif
#ifdef FGETS
      // to read nb_chars, necessary to pass nb_chars+1
      if (fgets(sread, d->nb_chars, file) != NULL ) {
         d->last_char = sread[0];
      }
      else {
         d->error_flag = 1;
      }
#endif
#ifdef FPUTS
      if (fputs(swrite, file) == EOF) {
         d->error_flag = 1;
      }
#endif
#ifdef FREAD
      if (fread(sread, 1, d->nb_chars, file)) {
         d->last_char = sread[d->nb_chars-1];
         // necessary to finish the string after fread
         sread[d->nb_chars] = '\0';
      }
      else {
         d->error_flag = 1;
      }
#endif
#ifdef FWRITE
      if (!fwrite(swrite, 1, d->nb_chars, file)) {
         d->error_flag = 1;
      }
#endif

#ifdef FGETCFPUTC
      if ((d->last_char = fgetc(file)) == EOF) {
         d->error_flag = 1;
      }
      if (fputc(c, file) == EOF) {
         d->error_flag = 1;
      }
#endif
#ifdef FGETSFPUTS
      // to read nb_chars, necessary to pass nb_chars+1
      if (fgets(sread, d->nb_chars+1, file) != EOF ) {
         d->last_char = sread[0];
      }
      else {
         d->error_flag = 1;
      }
      if (fputs(swrite, file) == EOF) {
         d->error_flag = 1;
      }
#endif
#ifdef FREADFWRITE
      if (fread(sread, 1, d->nb_chars, file)) {
         d->last_char = sread[d->nb_chars-1];
         // necessary to finish the string after fread
         sread[d->nb_chars] = '\0';
      }
      else {
         d->error_flag = 1;
      }
      if (!fwrite(swrite, 1, d->nb_chars, file)) {
         d->error_flag = 1;
      }
#endif

#ifdef FPUTCFGETC
      if (fputc(c, file) == EOF) {
         d->error_flag = 1;
      }
      if ((last_char = fgetc(file)) == EOF) {
         d->error_flag = 1;
      }
#endif
#ifdef FPUTSFGETS
      // to read nb_chars, necessary to pass nb_chars+1
      if (fputs(swrite, file) == EOF) {
         d->error_flag = 1;
      }
      if (fgets(sread, d->nb_chars+1, file) != EOF ) {
         d->last_char = sread[0];
      }
      else {
         d->error_flag = 1;
      }
#endif
#ifdef FWRITEFREAD
      if (!fwrite(swrite, 1, d->nb_chars, file)) {
         d->error_flag = 1;
      }
      if (fread(sread, 1, d->nb_chars, file)) {
         d->last_char = sread[d->nb_chars-1];
         // necessary to finish the string after fread
         sread[d->nb_chars] = '\0';
      }
      else {
         d->error_flag = 1;
      }
#endif
#endif
#if 1
      for(w=0, we=d->nb_rand; w < we; w++){
         d->last_char+=rand_r(&d->seed)%100;
      }
#endif
   }
#ifdef STATS
   _ITM_finalizeThread();
#endif

   free(sread);
   return NULL;
}

int main(int argc, char **argv)
{
   int i, j;

   int nb_it;
   int nb_threads;
   int nb_chars;
   int nb_rand;
   pthread_attr_t pthread_custom_attr;
   thread_data_t *data;
   struct timeval start, end, elapsed;
   int last_fseek = 0;
   int error_flag = 0;

//#ifdef PIN_THREADS
//   cpu_set_t cpuset;
//#endif

#if defined FGETS || defined FPUTS || defined FREAD || defined FWRITE || defined FGETSFPUTS || defined FREADFWRITE || defined FPUTSFGETS || defined FWRITEFREAD
   if (argc != 5 ) {
      printf("micro (number of iterations) (number of rand) (number of chars) (number of threads) \n");
      exit(1);
   }
   nb_it = atoi(argv[1]);
   nb_rand = atoi(argv[2]);
   nb_chars = atoi(argv[3]);
   nb_threads = atoi(argv[4]);
#else
   if (argc != 4 ) {
      printf("micro (number of iterations) (number of rand) (number of threads) \n");
      exit(1);
   }
   nb_it = atoi(argv[1]);
   nb_rand = atoi(argv[2]);
   nb_threads = atoi(argv[3]);
   nb_chars = 0;
#endif
   nb_it /= nb_threads;

   detrans_init();

   /* Open for reading and writing, whith the start position at the file. */
   file = fopen(filePath, "r+");

   if (file == NULL) {
      fprintf(stderr, "Error openning the file\n");
      exit(1);
   }

   // We use a barr to make worker threads start roughly at the same time.
   pthread_barrier_init(&barr, NULL, nb_threads + 1);

   // Start threads.
   threads = (pthread_t *)malloc(nb_threads * sizeof(pthread_t));
   if (threads == NULL) {
      perror("malloc");
      exit(1);
   }
   pthread_attr_init(&pthread_custom_attr);

   /* Create data for threads*/
   if ((data = (thread_data_t *)malloc(nb_threads * sizeof(thread_data_t))) == NULL) {
      perror("malloc");
      exit(1);
   }

   for (i = 0; i < nb_threads; i++) {
      char* s = (char*)malloc((nb_chars+1)*sizeof(char));
      for (j=0; j<nb_chars; j++) {
         s[j] = 's';
      }
      s[nb_chars]='\0';
      data[i].s = s;
      data[i].seed = rand();
      data[i].nb_chars = nb_chars;
      data[i].nb_it = nb_it;
      data[i].nb_rand = nb_rand;

#ifdef PIN_THREADS
      data[i].tid = i;
      //		CPU_ZERO(&cpuset);
      //		CPU_SET(i, &cpuset);
      //		pthread_attr_setaffinity_np(&pthread_custom_attr, sizeof(cpuset), &cpuset);
#endif

      if (pthread_create(&threads[i], &pthread_custom_attr, thread_function, (void *)(&data[i])) != 0) {
         fprintf(stderr, "Error creating thread\n");
         exit(1);
      }
   }

#ifdef USE_LOCK
      __sim_on();
#endif
   // Go into the barr, enables worker threads to start
   pthread_barrier_wait(&barr);

   // Start time
   gettimeofday(&start, NULL);

   // Wait for worker threads to stop
   for (i = 0; i < nb_threads; i++) {
      if (pthread_join(threads[i], NULL) != 0) {
         fprintf(stderr, "Error waiting for thread completion\n");
         exit(1);
      }
        if (data[i].error_flag && !data[i].last_fseek)
          error_flag |= 1;
   }

   // End time
   gettimeofday(&end, NULL);

#ifdef USE_LOCK
      __sim_off();
#endif

   free(threads);
   fclose(file);

   elapsed.tv_sec = end.tv_sec - start.tv_sec;
   elapsed.tv_usec = end.tv_usec - start.tv_usec;
   if (elapsed.tv_usec < 0) {
      elapsed.tv_sec--;
      elapsed.tv_usec += USECSPERSEC;
   }

   printf("\nTime = %d.%06d\n", elapsed.tv_sec, elapsed.tv_usec);

   if (error_flag && !last_fseek) {
      printf("Error in one/more functions.\n");
   }

   detrans_exit();
   return 0;
}
