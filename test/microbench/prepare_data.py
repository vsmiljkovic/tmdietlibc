#
# A Python script for reading a table from an input file.
# The table contains: - execution time of benchmarks,
#                      - columns as different TM implementations,
#                      - rows as different thread numbers.
# The output is a table of average execution time values excluding 2 max values,
# and normalized by the original single-threaded value (also average
# excluding 2 max values).
#
# I call this script from the script show_results.sh.

import sys

if (len(sys.argv) > 1):
  num_implementations = int(sys.argv[1:2][0])
  implementations = sys.argv[2:2+num_implementations]
  input_files = sys.argv[2+num_implementations:]
else:
  print "Usage: python prepare_data.py num_implementations implementations input_files"
  exit()

num_it = 1
normalize = 1 
speedup = 0 
threads = [1, 2, 4, 8]

original_impl = implementations.index('nondet')  # the index of the original (nondet) column
columns=implementations # only columns with data you want to show in a graph

#original_value = 0 # average original single-threaded execution time

table = []
# table will be:
#
# Threads HyTM STM STMIRR HyTM STM STMIRR 
# 1       0.3  0.5  0.4  0.4  0.5  0.4
# 2       0.5  0.7  0.6  0.5  1.1  0.5
# 4       0.7  1.1  0.8  0.6  2.1  0.7
# 8       0.6  1.5  0.7  0.3  2.2  0.4
table.append(["Threads"])#, "ORIGINAL", "SERIAL", "TMPARALLEL", "DTHREADS"])
for th in threads:
  table.append([th])

def geomean(numbers):
    product = 1
    for n in numbers:
        product *= n
    return product ** (1.0/len(numbers))


def get_average(num_row):
  for ic, column in enumerate(columns):
    if normalize:
      if speedup:
        value = original_value/a[ic][num_row]
      else:
        value = a[ic][num_row]/original_value
    else:
      value = a[ic][num_row]
    table[num_row+1].append(value)

for input_file in input_files:
  a = []
  for i in range(len(columns)):
    a.append([])

  for column in columns:
    table[0].append(column)
  
  for (i, each_line) in enumerate(open(input_file,'rb')):
    for ic, column in enumerate(columns):
      a[ic].append((float)(each_line.split('\t')[ic].strip()))
     
  original_value = a[original_impl][0]

  for (i, each_line) in enumerate(open(input_file,'rb')):
   get_average(i);

table[0].append("nondet")
table[0].append("det")

for i in range(1, 5):
  #print geomean(table[i][1::2]), geomean(table[i][2::2])
  gm1 = geomean(table[i][1::2])
  gm2 = geomean(table[i][2::2])
  table[i].append(gm1)
  table[i].append(gm2)
#  am1 = sum(table[i][1::2])/5
#  am2 = sum(table[i][2::2])/5
#  table[i].append(am1)
#  table[i].append(am2)

for row in table:
  for el in row:
    print el,
  print

#get the average values for 8 threads:
#for i in range(1, 5):
#  print "for threads", table[i][0],":", "det, nondet"
#  print sum(table[i][1::2])/4, sum(table[i][2::2])/4

