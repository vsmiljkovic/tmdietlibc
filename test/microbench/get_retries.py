#!/usr/bin/env python

# Usage: python vesna.py imefile

from __future__ import with_statement

import sys

ids = []
retries = []
tx_id = None

with open(sys.argv[1]) as a:
    for i in a:
        if 'Thread' in i:
            tx_id = int(i.split()[1])
            if tx_id == 0:
                ids.append(0)
                retries.append(0)
        if 'Retries' in i:
            r = int(i.split()[-1])
            ids[-1] += 1
            retries[-1] += r


for i, j, k in zip(ids[::2], retries[::2], retries[1::2]):
    # thread number, tx_aborts, tx_irevocable_aborts
    print i, j, k
