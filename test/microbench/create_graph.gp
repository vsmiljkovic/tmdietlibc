# input_file and output_file have to be defined in the command line
# E.g.:
#    gnuplot -e "input_file='stamp.perf'" -e "output_file='stamp.eps'" create_graph.gp

# I call this script from the script show_results.sh.

reset
set terminal postscript enhanced eps mono dashed lw 1 "Helvetica" 14
#set title "Evaluation"
set style fill solid 1.00 border 0.00
set style data histogram
set style histogram cluster gap 1
set boxwidth 0.95
unset grid
unset xtics
unset ytics
unset y2tics

set size 1,0.38

set xtics
set xlabel " " offset 0,-0.5
set ylabel "Slowdown" 
set ytics 1 
set grid ytics 

#set xrange [-2:25]
#set lmargin 4
#set rmargin 1
#set yrange [0:12]

set out output_file

set key autotitle columnhead

set style line 1 lc rgb '#C8FEC8' lt 1 lw 1 pt 5 ps 1.5   # --- color #4
set style line 2 lc rgb '#111111' lt 1 lw 1 pt 5 ps 1.5   # --- color #3
set style line 3 lc rgb '#DD9933' lt 1 lw 1 pt 5 ps 1.5   # --- color #2
set style line 4 lc rgb '#7F3F5F' lt 1 lw 1 pt 5 ps 1.5   # --- color #1

plot newhistogram "fgetc", \
input_file using 2:xtic(1) t 'original' ls 1, '' u 3 t 'DeTrans-lib' ls 2, \
newhistogram "fgets", \
'' u 4:xtic(1) notitle ls 1, '' u 5 notitle ls 2, \
newhistogram "fread", \
'' u 6:xtic(1) notitle ls 1, '' u 7 notitle ls 2, \
newhistogram "fputc", \
'' u 8:xtic(1) notitle ls 1, '' u 9 notitle ls 2, \
newhistogram "fputs", \
'' u 10:xtic(1) notitle ls 1, '' u 11 notitle ls 2, \
newhistogram "mean", \
'' u 12:xtic(1) notitle ls 1, '' u 13 notitle ls 2
