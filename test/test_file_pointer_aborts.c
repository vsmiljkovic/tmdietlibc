/*
 * Testing fseek, fgets, fputc, values of the file pointer
 * and conflicts among transactions.
 */
#include <stdio.h>
#include <assert.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <errno.h>
#include <dietstdio.h>
#include <unistd.h>
#include <sys/types.h>

#include <dietlibc-tanger-stm.h>
#include <TM_checker.h>

typedef struct thread_data {
  int nb_fseek_aborts;
  int nb_fgets_aborts;
} thread_data_t;

void count_fseek(thread_data_t *d) __attribute__ ((transaction_pure));
void count_fgets(thread_data_t *d) __attribute__ ((transaction_pure));

// Counting how many time a tx with fseek started execution
void count_fseek(thread_data_t *d) {
  d->nb_fseek_aborts++;
}

// Counting how many time a tx with fgets started execution
void count_fgets(thread_data_t *d) {
  d->nb_fgets_aborts++;
}

pthread_barrier_t barr;

int nb_threads = 4; // Number of threads
int nb_iterations = 16; // Number of iterations
long nb_it_total;
pthread_t *threads;
int test_fails = 0;
FILE* file;
char filePath[] = "./Test.txt";
char* sentence = "This function sets the file position indicator for the stream 'stream' to the position 'position', which must have been set by a previous call to fgetpos on the same stream. If successful, fsetpos clears the end-of-file indicator on the stream, discards any characters that were pushed back by the use of ungetc, and returns a value of zero. Otherwise, fsetpos returns a nonzero value and stores an implementation-defined positive value in errno. This function sets the file position indicator for the stream 'stream' to the position 'position', which must have been set by a previous call to fgetpos on the same stream. If successful, fsetpos clears the end-of-file indicator on the stream, discards any characters that were pushed back by the use of ungetc, and returns a value of zero. Otherwise, fsetpos returns a nonzero value and stores an implementation-defined positive value in errno. ";


// Worker thread function
void *thread_function(void *data)
{
    int i, pos;
    char buf[11];
    thread_data_t* d = (thread_data_t*)data;  
    int r = rand() % nb_it_total;

    // Go into the barr. This makes sure that threads start roughly at
    // the same time.
    pthread_barrier_wait(&barr);
    for (i = 0; i < nb_iterations; i++) {
    TRANSACTION_BEGIN
      count_fseek(d);
      fseek(file, r, SEEK_SET);
      file_pointer_checker(file, "After fseek");
      sleep(1);
    TRANSACTION_END

    TRANSACTION_BEGIN
      count_fgets(d);
      if(fgets(buf, 10, file) == NULL) {
         file_pointer_checker(file, "After fgets if it's NULL");
	 test_fails = 1;
	 return NULL;
      }
      file_pointer_checker(file, "After fgets if it's not NULL");
    TRANSACTION_END

      pos = strstr(sentence, buf) - sentence;
      if (pos < 0 || pos > 1000) {
	 test_fails = 1;
	 return NULL;
      }
    }
    return NULL;
}


int main(int argc, char **argv)
{
    volatile int i, j;
    nb_it_total = nb_threads*nb_iterations;
    thread_data_t * data = (thread_data_t *)malloc(nb_threads * sizeof(thread_data_t));
    file = fopen(filePath, "w+");

    file_pointer_checker(file, "After open with w+");

    if(file==NULL) {
	printf("Error: can't create file.\n");
	return 1;
    }	
    else {
	for (j = 0; j < nb_it_total; j++) {
	     for(i = 0; sentence[i] ; i++) {

              TRANSACTION_BEGIN
               fputc(sentence[i], file);
               file_pointer_checker(file, "After fputc");
              TRANSACTION_END
	     }
	}
	fclose(file);

	/* Open file for reading */
        TRANSACTION_BEGIN
	 file = fopen(filePath, "r+");
         file_pointer_checker(file, "After fopen with r+");
        TRANSACTION_END

	// We use a barr to make worker threads start roughly at the same time.
	pthread_barrier_init(&barr, NULL, nb_threads + 1);
	
	// Start threads.
	threads = (pthread_t *)malloc(nb_threads * sizeof(pthread_t));
	if (threads == NULL) {
		perror("malloc");
		exit(1);
	}

	for (i = 0; i < nb_threads; i++) {
		if (pthread_create(&threads[i], NULL, thread_function, (void*)(&(data[i]))) != 0) {
		    fprintf(stderr, "Error creating thread\n");
		    exit(1);
		}
	}

	// Go into the barr, enables worker threads to start
	pthread_barrier_wait(&barr);

	// Wait for worker threads to stop
	for (i = 0; i < nb_threads; i++) {
		if (pthread_join(threads[i], NULL) != 0) {
		    fprintf(stderr, "Error waiting for thread completion\n");
		    exit(1);
		}
	}
	free(threads);
	fclose(file);

	for (i = 0; i < nb_threads; i++) {
	 	printf("Thread %d, num_fseek_aborts = %d, num_fgets_aborts = %d\n", i, 
		data[i].nb_fseek_aborts - nb_iterations,
		data[i].nb_fgets_aborts - nb_iterations);
	}

   	if (!test_fails) {
	  printf("SUCCESS\n");
    	}
    	else {
	  printf("FAILURE\n");
   	}

	return 0;
    }
}
