#ifndef __GC_H__
#define __GC_H__

typedef struct gc_st gc_t;

/* Most of these functions peek into a per-thread state struct. */
#include "ptst.h"

/*
 * A chunk amortises the cost of allocation from shared lists. It also
 * helps when zeroing nodes, as it increases per-cacheline pointer density
 * and means that node locations don't need to be brought into the cache 
 * (most architectures have a non-temporal store instruction).
 */
//#define BLKS_PER_CHUNK 100
/* tm-dietlibc change */
#define BLKS_PER_CHUNK 1

typedef struct chunk_st chunk_t;
struct chunk_st
{
    chunk_t *next;             /* chunk chaining                 */
    unsigned int i;            /* the next entry in blk[] to use */
    void *blk[BLKS_PER_CHUNK];
};

/* Initialise GC section of given per-thread state structure. */
gc_t *gc_init(void);

int gc_add_allocator(int alloc_size);
void gc_remove_allocator(int alloc_id);

/*
 * Memory allocate/free. An unsafe free can be used when an object was
 * not made visible to other processes.
 */
void *gc_alloc(ptst_t *ptst, int alloc_id);
void gc_free(ptst_t *ptst, void *p, int alloc_id);
void gc_unsafe_free(ptst_t *ptst, void *p, int alloc_id);

/*
 * Hook registry. Allows users to hook in their own per-epoch delay
 * lists.
 */
typedef void (*hook_fn_t)(ptst_t *, void *);
int gc_add_hook(hook_fn_t fn);
void gc_remove_hook(int hook_id);
void gc_add_ptr_to_hook_list(ptst_t *ptst, void *ptr, int hook_id);

/* Per-thread entry/exit from critical regions */
void gc_enter(ptst_t *ptst);
void gc_exit(ptst_t *ptst);

/* Start-of-day initialisation of garbage collector. */
void _init_gc_subsystem(void);
void _destroy_gc_subsystem(void);

//#ifndef LOCK_MODE
chunk_t* compare_and_swap(chunk_t** head, chunk_t* h, chunk_t* ch);
ptst_t* compare_and_swap_ptst(ptst_t** head, ptst_t* h, ptst_t* ch);
unsigned int compare_and_swap_add( unsigned int* r, unsigned int oldval, unsigned int newval);
//#endif

#endif /* __GC_H__ */
