/******************************************************************************
 * set_harness.c
 * 
 * Test harness for the various set implementations.
 * 
 * Copyright (c) 2002-2003, K A Fraser
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <sys/resource.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#ifdef USE_LOCK
#include <sys/ucontext.h>
#endif
#include <signal.h>
#include <sched.h>
#include <limits.h>
#include <assert.h>
#include <stdarg.h>

#include "portable_defns.h"
#include "set.h"
#include "ptst.h"
#include "gc.h"

#include <dietlibc-tanger-stm.h>

/* This produces an operation log for the 'replay' checker. */
/*#define DO_WRITE_LOG*/

#ifdef DO_WRITE_LOG
#define MAX_ITERATIONS 100000
#define MAX_WALL_TIME 50 /* seconds */
#else
#define MAX_ITERATIONS 100000000
#define MAX_WALL_TIME 10 /* seconds */
#endif

/*
 * ***************** LOGGING
 */

#define MAX_LOG_RECORDS 256

#define LOG_KIND_INT 0
#define LOG_KIND_STRING 1
#define LOG_KIND_FLOAT 2

/* tm-dietlibc change */
#ifdef USE_LOCK
#define __printf_chk(flag, format) printf(flag, format)
#else
//static int tanger_wrapperpure_fprintf_lock(FILE *stream, const char *format, ...) __attribute__ ((weakref("fprintf_lock")));
static int tanger_wrapperpure_printf(FILE *stream, const char *format, ...) __attribute__ ((weakref("printf")));
//static pthread_t tanger_wrapperpure_pthread_self(void) __attribute__ ((weakref("pthread_self")));
#endif

static void tanger_wrapperpure_tanger_stm_intrinsic_barrier()
    __attribute__ ((weakref("tanger.llvm.memory.barrier")));

static int tanger_wrapperpure_tanger_stm_intrinsic_cas(int*, int, int)
    __attribute__ ((weakref("tanger.llvm.atomic.cmp.swap.i32.p0i32")));


/* tm-dietlibc change */
typedef struct thread_data {
  char pad1[64];
  int id;
  int nb_update0; // initial keys inserts
  int nb_update;
  int nb_remove;
  int nb_lookup;
  char pad2[64];
} thread_data_t;

thread_data_t *data;

typedef struct {
    char *name;
    int kind;
    int val_int;
    char *val_string;
    float val_float;
} log_record_t;

static log_record_t log_records[MAX_LOG_RECORDS];

static int num_log_records = 0;

static void log_int (char *name, int val) {
    log_records[num_log_records].name = name;
    log_records[num_log_records].kind = LOG_KIND_INT;
    log_records[num_log_records].val_int = val;
    num_log_records ++;
}

#ifdef USE_LOCK
static void log_string (char *name, char *val) {
    log_records[num_log_records].name = name;
    log_records[num_log_records].kind = LOG_KIND_STRING;
    log_records[num_log_records].val_string = val;
    num_log_records ++;
}
#endif

static void log_float (char *name, float val) {
    log_records[num_log_records].name = name;
    log_records[num_log_records].kind = LOG_KIND_FLOAT;
    log_records[num_log_records].val_float = val;
    num_log_records ++;
}

static void dump_log (void) {
    int i;

    fprintf_lock (stdout, "-------------------------------------------"
             "---------------------------\n");
    for (i = 0; i < num_log_records; i ++)
    {
        char padding[40];
        strcpy(padding, "                                        ");
        if (30-strlen(log_records[i].name) >= 0){
            padding[30-strlen(log_records[i].name)] = '\0';
        }
        fprintf_lock (stdout, "%s%s = ", padding, log_records[i].name);
        {
            int kind = log_records [i].kind;
            if (kind == LOG_KIND_INT) {
                fprintf_lock (stdout, "%d\n", log_records[i].val_int);
            } else if (kind == LOG_KIND_STRING) {
                fprintf_lock (stdout, "%s\n", log_records[i].val_string);
            } else if (kind == LOG_KIND_FLOAT) {
                fprintf_lock (stdout, "%.3f\n", log_records[i].val_float);
            } 
        }
    }
    fprintf_lock (stdout, "-------------------------------------------"
             "---------------------------\n");

    for (i = 0; i < num_log_records; i ++)
    {
        int kind = log_records [i].kind;
        if (i != 0) { fprintf_lock (stderr, " "); }
        if (kind == LOG_KIND_INT) {
            fprintf_lock (stderr, "%d", log_records[i].val_int);
        } else if (kind == LOG_KIND_STRING) {
            fprintf_lock (stderr, "%s", log_records[i].val_string);
        } else if (kind == LOG_KIND_FLOAT) {
            fprintf_lock (stderr, "%.3f", log_records[i].val_float);
        } 
    }
    fprintf_lock (stderr, " LOG\n");
}

/*
 * ************** END OF LOGGING
 */

#define TVAL(x) ((x.tv_sec * 1000000) + x.tv_usec)

/* Log tables. Written out at end-of-day. */
typedef struct log_st
{
    interval_t    start, end;
    unsigned int  key;
    void         *val, *old_val; /* @old_val used by update() and remove() */
} log_t;
#define SIZEOF_GLOBAL_LOG (num_threads*MAX_ITERATIONS*sizeof(log_t))
static log_t *global_log;
static interval_t interval = 0;

static volatile bool_t go = FALSE;
static volatile unsigned int threads_initialised1 = 0, max_key, log_max_key;
static volatile unsigned int threads_initialised2 = 0;
static volatile unsigned int threads_initialised3 = 0;

//static bool_t go = FALSE;
//static int threads_initialised1 = 0, max_key, log_max_key;
//static int threads_initialised2 = 0;
//static int threads_initialised3 = 0;
static int num_threads;
static int num_iterations;
static float num_it_per_thread;

static unsigned long proportion;

static struct timeval start_time, done_time;
static struct tms start_tms, done_tms;
static float wall_time, user_time, sys_time;

static int successes[MAX_THREADS];

#ifdef SPARC
static int processors[MAX_THREADS];
#endif

/* All the variables accessed in the critical main loop. */
static struct {
    CACHE_PAD(0);
    bool_t alarm_time;
    CACHE_PAD(1);
    set_t *set;
    CACHE_PAD(2);
} shared;

#define nrand(_r) (((_r) = (_r) * 1103515245) + 12345)

static void alarm_handler( int arg)
{
    shared.alarm_time = 1;
}

/*int cntr[MAX_THREADS] = { 0 };*/

static void *thread_start(void *arg)
{
    unsigned long k;
    int i;
    void *ov, *v;
    thread_data_t *d = (thread_data_t *)arg;
    int id = d->id;
#ifdef DO_WRITE_LOG
    log_t *log = global_log + id*MAX_ITERATIONS;
    interval_t my_int;
#endif
    unsigned long r = ((unsigned long)arg)+3; /*RDTICK();*/
    unsigned int prop = proportion;
    unsigned int _max_key = max_key;

    pin_cpu(id);

    if ( id == 0 )
    {
        _init_ptst_subsystem();
        _init_gc_subsystem();
        _init_set_subsystem();
        shared.set = set_alloc();
    }
    /* BARRIER FOR ALL THREADS */
    {
        int n_id, id = threads_initialised1;
        while ( (n_id = CAS(&threads_initialised1, id, id+1)) != id )
    		id = n_id;
    }
    while ( threads_initialised1 != num_threads ) MB();

#ifndef DO_WRITE_LOG
    /* Start search structure off with a well-distributed set of inital keys */
    for ( i = (_max_key / num_threads); i != 0; i >>= 1 )
    {
        for ( k = i >> 1; k < (_max_key / num_threads); k += i )
        {
            set_update(shared.set, 
                       k + id * (_max_key / num_threads), 
                       (void *)0xdeadbee0, 1);
            d->nb_update0++;
        } 
    }
#endif

    {
        int n_id, id = threads_initialised2;
	while ( (n_id = CAS(&threads_initialised2, id, id+1)) != id )
            id = n_id;
    }
    while ( threads_initialised2 != num_threads ) MB();

    if ( id == 0 )
    {
        (void)signal(SIGALRM, &alarm_handler);
        (void)alarm(MAX_WALL_TIME);
        WMB();
        gettimeofday(&start_time, NULL);
        times(&start_tms);
        go = TRUE;
        WMB();
    } 
    else 
    {
        while ( !go ) MB();
    }

#ifdef DO_WRITE_LOG
    get_interval(my_int);
#endif

    for ( i = 0; /*(*/i < num_it_per_thread/*) && !shared.alarm_time*/; i++ ) 
    {
        /* O-3: ignore ; 4-11: proportion ; 12: ins/del */
        k = (nrand(r) >> 4) & (_max_key - 1);
#ifdef DO_WRITE_LOG
        log->start = my_int;
#endif
        if ( ((r>>4)&255) < prop )
        {
            ov = v = set_lookup(shared.set, k);
	    d->nb_lookup++;
        }
        else if ( ((r>>12)&1) )
        {
            v = (void *)((r&~7)|0x8);
            ov = set_update(shared.set, k, v, 1);
	    d->nb_update++;
        }
        else
        {
            v = NULL;
            ov = set_remove(shared.set, k);
	    d->nb_remove++;
        }

#ifdef DO_WRITE_LOG
        get_interval(my_int);
        log->key = k;
        log->val = v;
        log->old_val = ov;
        log->end = my_int;
        log++;
#endif

    }

    /* BARRIER FOR ALL THREADS */
    {
        int n_id, id = threads_initialised3;
	while ( (n_id = CAS(&threads_initialised3, id, id+1)) != id ) 
            id = n_id;
    }
    while ( threads_initialised3 != num_threads ) MB();

#if 0
    if ( id == 0 )
    {
        extern void check_tree(set_t *);
        check_tree(shared.set);
    }
#endif

    if ( id == num_threads - 1 )
    {
        gettimeofday(&done_time, NULL);
        times(&done_tms);
        WMB();
        _destroy_gc_subsystem();
    } 

    successes[id] = i;
  
#ifdef STATS
    _ITM_finalizeThread();
#endif
    return(NULL);
}

#define THREAD_TEST thread_start
#define THREAD_FLAGS THR_BOUND

#ifdef PPC
static pthread_attr_t attr;
#endif

static void test_multithreaded (void)
{
    int                 i;
    pthread_t            thrs[MAX_THREADS];
    int num_successes;
    int min_successes, max_successes;
    int ticksps = sysconf(_SC_CLK_TCK);

    if ( num_threads == 1 ) goto skip_thread_creation;

#ifdef PPC
    i = pthread_attr_init (&attr);
    if (i !=0) {
        fprintf_lock (stderr, "URK!  pthread_attr_init rc=%d\n", i);
    }
    i = pthread_attr_setscope (&attr, PTHREAD_SCOPE_SYSTEM);
    if (i !=0) {
        fprintf_lock (stderr, "URK!  pthread_attr_setscope rc=%d\n", i);
    }
#endif

/*tm-dietlibc change
#ifdef MIPS
    pthread_setconcurrency(num_threads + 1);
#else
    pthread_setconcurrency(num_threads);
#endif
*/

#ifdef USE_LOCK
      __sim_on();
#endif

    for (i = 0; i < num_threads; i ++)
    {
#ifndef USE_LOCK
        MB();
#endif
		/* tm-dietlibc change */
        data[i].id = i;
 
#ifdef PPC
        pthread_create (&thrs[i], &attr, THREAD_TEST, (void *)(&data[i]));
#else
        pthread_create (&thrs[i], NULL, THREAD_TEST, (void *)(&data[i]));
#endif
    }

 skip_thread_creation:
    if ( num_threads == 1 )
    {
#ifdef USE_LOCK
      __sim_on();
#endif
 	// tm-dietlibc change thread_start(0);
	/* tm-dietlibc change */
        data[0].id = 0;
        thread_start(&data[0]);
    }
    else
    {
        for (i = 0; i < num_threads; i ++)
        {
            (void)pthread_join (thrs[i], NULL);
        }
    }

#ifdef USE_LOCK
      __sim_off();
#endif

    wall_time = (float)(TVAL(done_time) - TVAL(start_time))/ 1000000;
    user_time = ((float)(done_tms.tms_utime - start_tms.tms_utime))/ticksps;
    sys_time  = ((float)(done_tms.tms_stime - start_tms.tms_stime))/ticksps;

//    log_float ("wall_time_s", wall_time);
    log_float ("Time", wall_time); /* TM-dietlibc change in order to be compatible with other bench for the evaluation */
    log_float ("user_time_s", user_time);
    log_float ("system_time_s", sys_time);

    num_successes = 0;
    min_successes = INT_MAX;
    max_successes = INT_MIN;
    for ( i = 0; i < num_threads; i++ )
    {
        num_successes += successes[i];
        if ( successes[i] < min_successes ) min_successes = successes[i];
        if ( successes[i] > max_successes ) max_successes = successes[i];
    }

    log_int ("min_successes", min_successes);
    log_int ("max_successes", max_successes);
    log_int ("num_successes", num_successes);

    log_float("us_per_success", (num_threads*wall_time*1000000.0)/num_successes);

    log_int("log max key", log_max_key);
}

#if defined(INTEL)
static void tstp_handler(int sig, siginfo_t *info, ucontext_t *uc)
{
    static unsigned int sem = 0;
/* tm-dietlibc change
    unsigned long *esp = (unsigned long *)(uc->uc_mcontext.gregs[7]);
*/
    int pid = getpid();

    while ( CAS(&sem, 0, 1) != 0 ) sched_yield();

printf("Address of function level 0: %x, level 1: %x \n", __builtin_return_address(0), __builtin_return_address(1));

    printf("Signal %d for pid %d\n", sig, pid);
/* tm-dietlibc change
    printf("%d: EIP=%08x  EAX=%08x  EBX=%08x  ECX=%08x  EDX=%08x\n", pid,
           uc->uc_mcontext.gregs[14], uc->uc_mcontext.gregs[11],
           uc->uc_mcontext.gregs[ 8], uc->uc_mcontext.gregs[10],
           uc->uc_mcontext.gregs[ 9]);
    printf("%d: ESP=%08x  EBP=%08x  ESI=%08x  EDI=%08x  EFL=%08x\n", pid,
           uc->uc_mcontext.gregs[ 7], uc->uc_mcontext.gregs[ 6],
           uc->uc_mcontext.gregs[ 5], uc->uc_mcontext.gregs[ 4],
           uc->uc_mcontext.gregs[16]);
*/
    printf("\n");

    sem = 0;

    for ( ; ; ) sched_yield();
}
#endif

int main (int argc, char **argv)
{
    int updates0 = 0;
    int updates = 0;
    int removes = 0;
    int lookups = 0;
//_ITM_initializeProcess();

    if ( argc != 5 )
    {
        printf("%s <key_power> <read_proportion> <num_iterations> <num_threads>\n"
               "(0 <= read_proportion <= 256)\n", argv[0]);
        exit(1);
    }

    memset(&shared, 0, sizeof(shared));

    log_max_key = atoi(argv[1]);
    max_key = 1 << atoi(argv[1]);
    log_int("max_key", max_key);

    proportion = atoi(argv[2]);
    log_float ("frac_reads", (float)proportion/256.0);

    num_iterations = atoi(argv[3]);

    num_threads = atoi(argv[4]);
    log_int ("num_threads", num_threads);

    num_it_per_thread = num_iterations/num_threads;

    log_int ("max_iterations", MAX_ITERATIONS);
    log_int ("wall_time_limit_s", MAX_WALL_TIME);

    data = (thread_data_t *)malloc(num_threads * sizeof(thread_data_t));
    if (data == NULL) {
      perror("malloc");
      exit(1);
    }

#if defined(INTEL)
    {
        struct sigaction act;
        memset(&act, 0, sizeof(act));
//        act.sa_handler = (void *)tstp_handler;
        act.sa_flags = SA_SIGINFO;
        sigaction(SIGTSTP, &act, NULL);
        sigaction(SIGQUIT, &act, NULL);
        sigaction(SIGSEGV, &act, NULL);
    }
#endif

    int i;

    test_multithreaded ();

    dump_log ();

#ifdef DO_WRITE_LOG
    printf("Writing log...\n");
    /* Write logs to data file */
    int fd = open(argv[4], O_WRONLY | O_CREAT | O_TRUNC, 0644);
    if ( fd == -1 )
    {
        fprintf_lock(stderr, "Error writing log!\n");
        exit(-1);
    }

    if ( (write(fd, log_header, sizeof(log_header)) != sizeof(log_header)) ||
         (write(fd, global_log, SIZEOF_GLOBAL_LOG) != SIZEOF_GLOBAL_LOG) )
    {
        fprintf_lock(stderr, "Log write truncated or erroneous\n");
        close(fd);
        exit(-1);
    }

    close(fd);
#endif

    for (i = 0; i < num_threads; i++) {
      updates0 += data[i].nb_update0;
      updates += data[i].nb_update;
      removes += data[i].nb_remove;
      lookups += data[i].nb_lookup;
//      printf("Thread %i, updates0 = %d, updates = %d, removes = %d, lookups = %d\n", i, updates0, updates, removes, lookups);
    }
    free(data);

// for the input key_power=2, read_proportion=20, num_iterations= 1000, and num_threads= 2 => updates + removes + lookups should be 1006
// for the input key_power=2, read_proportion=20, num_iterations= 1000, and num_threads= 4 => updates + removes + lookups should be 1004
// for the input key_power=2, read_proportion=20, num_iterations= 1000, and num_threads= 8 => updates + removes + lookups should be 1000

printf("key= %d, read_proportion= %d, num_iterations= %d, and num_threads= %d\n", max_key, atoi(argv[2]), atoi(argv[3]), atoi(argv[4]));
//printf("updates = %d, removes = %d, lookups = %d\n", updates, removes,  lookups);

   if (atoi(argv[1]) == 2 && atoi(argv[2]) == 20 && atoi(argv[3]) == 1000) {
     if ((atoi(argv[4]) == 2 && updates + updates0 + removes + lookups == 1006) ||
          (atoi(argv[4]) == 4 && updates + updates0 + removes + lookups == 1004) ||
          (atoi(argv[4]) == 8 && updates + updates0 + removes + lookups == 1000) ) {
       printf("SUCCESS\n");
     }
     else  {
       printf("FAILURE\n");
     }
   }
/*   else {
      printf("FAILURE\n");
   }*/
   exit(0);
}

