/**
 * Function for testing TM transformation of the code.
**/

#include <TM_checker.h>
#include <assert.h>
#include <dietstdio.h>
#include <unistd.h>
#include <dietlibc-tanger-stm.h>
#include <sys/types.h>

extern short commit_var;

static void commit() __attribute__ ((transaction_pure));
static void commit() {
   if (commit_var == 0) commit_var = 1;
}
void TM_checker() {
   TRANSACTION_BEGIN
//   REGISTER_DEFERRAL(commit, NULL);
   counter_checker++;
//   SET_SYSCALLSAFE;
//   sleep(1);
   counter_checker += getpid();
//   counter_checker++;
   TRANSACTION_END

   TRANSACTION_BEGIN
//   SET_SYSCALLSAFE;
   counter_checker++;
   commit();
   TRANSACTION_END

}

void file_pointer_checker(FILE* stream, char* s) {
#if defined USE_TM || defined USE_TMIRR
//   printf("File pointer checker - %s\n", s);
   assert (lseek(stream->fd,0,SEEK_CUR) == stream->sys_position);
#endif
}
