/*
 * Testing malloc, free and conflicts among transactions.
 */
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <dietstdio.h>
#include <unistd.h>
#include <sys/types.h>

#include <dietlibc-tanger-stm.h>

/* tm-dietlibc change */
typedef struct thread_data {
  int id;
  int nb_first_malloc;
  int nb_second_malloc;
  int nb_free;
} thread_data_t;

pthread_barrier_t barr;

int nb_threads = 3; // Number of threads
int nb_iterations = 2; // Number of iterations
pthread_t *threads;
int test_fails = 0;

void first_f() __attribute__ ((transaction_pure));

void first_malloc_counter(thread_data_t* d) __attribute__ ((transaction_pure));
void second_malloc_counter(thread_data_t* d) __attribute__ ((transaction_pure));
void free_counter(thread_data_t* d) __attribute__ ((transaction_pure));

void first_f() {
  printf("Begin of thread_function\n");
}

void first_malloc_counter(thread_data_t* d) {
  (d->nb_first_malloc)++;
}

void second_malloc_counter(thread_data_t* d) {
  (d->nb_second_malloc)++;
}

void free_counter(thread_data_t* d) {
  (d->nb_free)++;
}

// Worker thread function
void *thread_function(void *data)
{
    int i;
    int *buf, *buf2;    

    thread_data_t *d = (thread_data_t *)data;

    // Go into the barr. This makes sure that threads start roughly at
    // the same time.
    pthread_barrier_wait(&barr);

    TRANSACTION_BEGIN
     first_malloc_counter(d);
     buf = (int*) malloc(nb_iterations*10*sizeof(int));
    TRANSACTION_END

    TRANSACTION_BEGIN
     first_malloc_counter(d);
     buf2 = (int*) malloc(26712*nb_iterations*sizeof(int));
    TRANSACTION_END

    for (i = 0; i < nb_iterations; i++) {
        buf[i] = i;
	buf2[i] = i;
	if (buf[i] != i && buf2[i] != i) {
	    test_fails = 1;
	    break;
	}
    }

    TRANSACTION_BEGIN
     free_counter(d);
     free(buf);
     free(buf2);
    TRANSACTION_END

    return NULL;
}


int main(int argc, char **argv)
{
    int i;

    thread_data_t * data = (thread_data_t *)malloc(nb_threads * sizeof(thread_data_t));

    // We use a barr to make worker threads start roughly at the same time.
    pthread_barrier_init(&barr, NULL, nb_threads + 1);
    threads = (pthread_t *)malloc(nb_threads * sizeof(pthread_t));
    if (threads == NULL) {
        perror("malloc");
        exit(1);
    }

    for (i = 0; i < nb_threads; i++) {
	data[i].id = i;
        if (pthread_create(&threads[i], NULL, thread_function, (void*)(&(data[i]))) != 0) {
            fprintf(stderr, "Error creating thread\n");
            exit(1);
        }
    }

    // Go into the barr, enables worker threads to start
    pthread_barrier_wait(&barr);

    // Wait for worker threads to stop
    for (i = 0; i < nb_threads; i++) {
        if (pthread_join(threads[i], NULL) != 0) {
            fprintf(stderr, "Error waiting for thread completion\n");
            exit(1);
        }
    }
    free(threads);

    for (i = 0; i < nb_threads; i++) {
       printf("-------- Thread %d -------- \n", i);
       printf("    - num of aborts in the first malloc = %d\n", data[i].nb_first_malloc-1);
       printf("    - num of aborts in the second malloc = %d\n", data[i].nb_second_malloc-1);
       printf("    - num of aborts in the free = %d\n", data[i].nb_free-1);
    }

    if (!test_fails) {
	printf("SUCCESS\n");
    }
    else {
	printf("FAILURE\n");
    }

    return 0;
}
