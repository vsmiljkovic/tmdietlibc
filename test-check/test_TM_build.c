/*
 * Testing TM and dietlibc compilation with TM tools.
 */
#include <stdio.h>
#include <assert.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <errno.h>
#include <dietstdio.h>
#include <unistd.h>
#include <sys/types.h>

#include <TM_checker.h>

#include <dietlibc-tanger-stm.h>


//pthread_barrier_t barr;

int nb_threads = 1; // Number of threads
int nb_iterations = 1; // Number of iterations
pthread_t *threads;
int test_fails = 0;


// Worker thread function
void *thread_function(void *data)
{
//    pthread_barrier_wait(&barr);

 //   TRANSACTION_BEGIN
    TM_checker();
 //   TRANSACTION_END

    return NULL;
}

int main(int argc, char **argv)
{
    volatile int i;

    // We use a barr to make worker threads start roughly at the same time.
//    pthread_barrier_init(&barr, NULL, nb_threads + 1);
	
    // Start threads.
    threads = (pthread_t *)malloc(nb_threads * sizeof(pthread_t));
    if (threads == NULL) {
        perror("malloc");
        exit(1);
    }

    for (i = 0; i < nb_threads; i++) {
        if (pthread_create(&threads[i], NULL, thread_function, NULL) != 0) {
            fprintf(stderr, "Error creating thread\n");
            exit(1);
        }
    }

//thread_function(NULL);

    // Go into the barr, enables worker threads to start
//    pthread_barrier_wait(&barr);

    // Wait for worker threads to stop
    for (i = 0; i < nb_threads; i++) {
        if (pthread_join(threads[i], NULL) != 0) {
            fprintf(stderr, "Error waiting for thread completion\n");
            exit(1);
        }
    }
    free(threads);

  //  if (!test_fails && counter_checker == 2*nb_threads*nb_iterations && commit_var != 0) {
	printf("SUCCESS\n");
  //  }
   // else {
	//printf("FAILURE\n");
   // }

    return 0;
}
