/*
 * Testing tx malloc.
 */
#include <stdio.h>
#include <assert.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <errno.h>
#include <dietstdio.h>
#include <unistd.h>
#include <sys/types.h>

#include <dietlibc-tanger-stm.h>

pthread_barrier_t barr;

int nb_threads = 8; // Number of threads
int nb_iterations = 10000; // Number of iterations
pthread_t *threads;
int test_fails = 0;

// Worker thread function
void *thread_function(void *data)
{
    int i;
    int** buf;

    // Go into the barr. This makes sure that threads start roughly at
    // the same time.
    pthread_barrier_wait(&barr);
    buf = (int**) malloc(nb_iterations*sizeof(int*));

    for (i = 0; i < nb_iterations; i++) {
        TRANSACTION_BEGIN;
        buf[i] = (int*) malloc(nb_iterations*sizeof(int));
        buf[i][i] = i;
        TRANSACTION_END;
    }



    for (i = 0; i < nb_iterations; i++) {
        if (buf[i][i] != i) {
            test_fails = 1;
            break;
        }
        TRANSACTION_BEGIN;
        free(buf[i]);
        TRANSACTION_END;
    }

    return NULL;
}


int main(int argc, char **argv)
{
    volatile int i;

    // We use a barr to make worker threads start roughly at the same time.
    pthread_barrier_init(&barr, NULL, nb_threads + 1);
    threads = (pthread_t *)malloc(nb_threads * sizeof(pthread_t));
    if (threads == NULL) {
        perror("malloc");
        exit(1);
    }

    for (i = 0; i < nb_threads; i++) {
        if (pthread_create(&threads[i], NULL, thread_function, NULL) != 0) {
            fprintf(stderr, "Error creating thread\n");
            exit(1);
        }
    }

    // Go into the barr, enables worker threads to start
    pthread_barrier_wait(&barr);

    // Wait for worker threads to stop
    for (i = 0; i < nb_threads; i++) {
        if (pthread_join(threads[i], NULL) != 0) {
            fprintf(stderr, "Error waiting for thread completion\n");
            exit(1);
        }
    }
    free(threads);

    if (!test_fails) {
	printf("SUCCESS\n");
    }
    else {
	printf("FAILURE\n");
    }

    return 0;
}
