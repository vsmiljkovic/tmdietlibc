/*
 *   Testing:
 * 1) Single-threaded execution of a transaction with a lib call does not abort.
 * 2) Multi-threaded execution causes aborts and calls of the undo handler.
 * 2) Parameters to the undo handler are saved and read corectly. 
 */

#include <stdio.h>
#include <assert.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <errno.h>
#include <dietstdio.h>
#include <unistd.h>
#include <sys/types.h>

#include <dietlibc-tanger-stm.h>


pthread_barrier_t barr;

int nb_threads = 4; // Number of threads
pthread_t *threads;
FILE* file;
char filePath[] = "./Test.txt";

int counter = 0;
int undo_calls = 0;

typedef void (* _ITM_userUndoFunction)(void *);
void _ITM_addUserUndoAction(_ITM_userUndoFunction, void *) __attribute__ ((transaction_pure));


struct a {
 pid_t pid;
 int pid_correct;
};

typedef struct a param;

void new_undo(param *args) __attribute__ ((transaction_pure));
void new_undo(param *args) {
  undo_calls = 1;

/* 
 * Part 3: Parameters to undo handlers should be saved and read correctly.
 */
  if (args != NULL) {
    if (args->pid == getpid()) {
      args->pid_correct = 1;
    }
    else {
      args->pid_correct = 0;
    }
  }
}

// Worker thread function
void *thread_function(void *data)
{
    param *p = (param*) data;
    p->pid = getpid();
    p->pid_correct = -1;

    pthread_barrier_wait(&barr);

    TRANSACTION_BEGIN

    REGISTER_UNDO((_ITM_userUndoFunction)new_undo, p);

    fseek(file, 10 + counter, SEEK_SET);
    sleep(3);
    counter++;

    TRANSACTION_END
   
    return NULL;
}


int main(int argc, char **argv)
{
	volatile int i;
	param *params = (param*)malloc(nb_threads * sizeof(param));
	param main_param;
	int test_fails = 0;

	/* Open file for reading */
	file = fopen(filePath, "r+");

/* 
 * Part 1: A single threaded execution should not have any transaction abort.
 * Due to separated tanger pass of library files and an example, it happend once
 * that because of the sys library call fseek, transaction was aborted and
 * executed in irrevocable mode.
 */
	thread_function(&main_param);

	if (undo_calls) {
	   test_fails = 1;
	}

/* 
 * Part 2: Multi-threaded execution should couse conflicts and aborted
 * transaction.
 */

	// We use a barr to make worker threads start roughly at the same time.
	pthread_barrier_init(&barr, NULL, nb_threads + 1);
	
	// Start threads.
	threads = (pthread_t *)malloc(nb_threads * sizeof(pthread_t));
	if (threads == NULL) {
		perror("malloc");
		exit(1);
	}

	for (i = 0; i < nb_threads; i++) {
		if (pthread_create(&threads[i], NULL, thread_function, &(params[i])) != 0) {
		    fprintf(stderr, "Error creating thread\n");
		    exit(1);
		}
	}

	// Go into the barr, enables worker threads to start
	pthread_barrier_wait(&barr);

	// Wait for worker threads to stop
	for (i = 0; i < nb_threads; i++) {
		if (pthread_join(threads[i], NULL) != 0) {
		    fprintf(stderr, "Error waiting for thread completion\n");
		    exit(1);
		}
	}

	printf("Counter = %d\n", counter);

	free(threads);
	fclose(file);
	
	if (!undo_calls) {
	   test_fails = 1;
	}
	else {
	   for (i = 0; i < nb_threads; i++) {
		if (params[i].pid_correct == 0) {
		    test_fails = 1;	
		}
	   }
	}

   	if (!test_fails) {
	  printf("SUCCESS\n");
    	}
    	else {
	  printf("FAILURE\n");
   	}

	return 0;
}
