extern int errno;

__attribute__ ((transaction_pure))
int *__errno_location(void) __attribute__((weak));

int *__errno_location() { return &errno; }

