#include <setjmp.h>
#include <signal.h>

void __longjmp(void*env,int val);
static void tanger_wrapperpure___longjmp(void*env,int val) __attribute__ ((weakref("__longjmp")));
static int tanger_wrapperpure_kill(pid_t pid, int sig) __attribute__ ((weakref("kill")));
static int tanger_wrapperpure_sched_yield(void) __attribute__ ((weakref("sched_yield")));
static int tanger_wrapperpure___sigsetjmp(jmp_buf __env,int __savemask) __attribute__ ((weakref("__sigsetjmp")));
static int tanger_wrapperpure___rt_sigprocmask(int how, const sigset_t *set, sigset_t *oldsetm, long nr) __attribute__ ((weakref("__rt_sigprocmask")));
static int tanger_wrapperpure___rt_sigsuspend(const sigset_t *mask, long nr) __attribute__ ((weakref("__rt_sigsuspend")));

void __libc_longjmp(sigjmp_buf env,int val);
void  __attribute__((noreturn)) __libc_longjmp(sigjmp_buf env,int val) {
  if (env[0].__mask_was_saved) {
    sigprocmask(SIG_SETMASK,(sigset_t*)&env[0].__saved_mask,0);
  }
  if (val==0) val=1;
  __longjmp(env[0].__jmpbuf,val);
}
void __siglongjmp(sigjmp_buf env,int val) {
  __libc_longjmp(env, val);
}
void longjmp(sigjmp_buf env,int val) __attribute__((weak,alias("__libc_longjmp")));
void  __attribute__((noreturn)) siglongjmp(sigjmp_buf env,int val)  {
  __libc_longjmp(env, val);
}
