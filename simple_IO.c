#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <detrans.h>

FILE *fp;
pthread_barrier_t barr;

void* thread_function(void* arg) {
  unsigned int i, num_chars = 3;
  char* buf = (char*) malloc (3*sizeof(char));
  pthread_t tid = pthread_self();
  pthread_barrier_wait(&barr);
  
  if (fread (buf, 1, 3, fp) != num_chars) {
    fprintf_lock (stderr, "l%d: fread() failed\n", __LINE__);
  }

  for (i = 0; i < num_chars; i++) {
    fprintf_lock(stderr, "%c", buf[i]);
  }
  fprintf_lock(stderr, "\n");
  return NULL;
}


int main() {
  unsigned int i;
  unsigned int N = 2;
  pthread_t t[N];
  pthread_t tid = pthread_self();

  detrans_init();

  pthread_barrier_init(&barr, NULL, N+1);

  fp = fopen ("simple_file.txt", "r");
  if (fp == NULL) {
    fprintf_lock (stderr, "%d: fopen() failed\n\n", __LINE__);
    return 1;
  }

  for (i = 0; i < N; i++) {
    fprintf_lock(stderr, "thread %ld\n", tid);
    pthread_create(&t[i], NULL, thread_function, NULL);
  }

  pthread_barrier_wait(&barr);

  for (i = 0; i < N; i++) {
    pthread_join(t[i], NULL);
  }

  fprintf_lock(stderr, "after pthread_join\n");

  fclose(fp);

  detrans_exit();

  return 0;
}
