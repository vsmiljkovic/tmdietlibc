#include <stdio.h>
#include <libitm.h>
#include <pthread.h>
#include <detrans.h>

int counter;
int tmp;


void* thread_function1(void* arg) {
  int tid = pthread_self();
  __transaction_atomic {
    fprintf_lock(stderr, "Thread %ld in f1: counter = %d\n", tid, counter); // -2
    counter++;
    fprintf_lock(stderr, "Thread %ld in f1: counter = %d\n", tid, counter); // -1
    _ITM_changeTransactionMode(modeSerialIrrevocable, NULL);
    fprintf_lock(stderr, "Thread %ld in f1: counter = %d (after set_irr)\n", tid, counter); // -1
  }
  fprintf_lock(stderr, "Thread %ld in f1: counter = %d\n", tid, counter); // -3

  return NULL;
}

void* thread_function2(void* arg) {
  int tid = pthread_self();
  __transaction_atomic {
    counter*=3;
    fprintf_lock(stderr, "Thread %ld in f2: counter = %d\n", tid, counter); // -6 and -3
    _ITM_changeTransactionMode(modeSerialIrrevocable, NULL);
    fprintf_lock(stderr, "Thread %ld in f2: counter = %d (after set_irr)\n", tid, counter);
  }
  fprintf_lock(stderr, "Thread %ld in f2: counter = %d\n", tid, counter); // -3
  return NULL;
}


int main() {
  pthread_t t[2];
  counter = -2;

  detrans_init();

  pthread_create(&t[0], NULL, thread_function1, NULL);
  pthread_create(&t[1], NULL, thread_function2, NULL);
 
  pthread_join(t[0], NULL);
  pthread_join(t[1], NULL);

  printf("Program ends here, counter=%d\n", counter);
  detrans_exit();
  return 0;
}

