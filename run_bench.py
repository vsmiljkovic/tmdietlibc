import filecmp
import datetime

time_det_dir       = './time-det/'
time_nondet_dir    = './time-nondet/'
time_serial_dir    = './time-serial/'

run_tiotest = 1 

if run_tiotest == 1:
  benchmarks = {
    'tiotest': '-f8 -b128 -t',
  }
else:
  benchmarks = {
    'micro_FGETC': '2000000 250 ',
    'micro_FREAD': '2000000 250 1 ',
    'micro_FGETS': '2000000 250 2 ',
    'micro_FPUTC': '2000000 250 ',
    'micro_FPUTS': '2000000 250 2 ',
  }

threads = [1, 2, 4, 8]

nb_iterations = 10

now = datetime.datetime.now()

out = now.strftime("%Y-%m-%d-%H-%M")
out_all    = out + "_all"       # in this file we'll put all the results
out_parsed = out + "_parsed"    # in this file we'll put parsed results


# a function for running bench, measuring time and parsing the results
def measure_time_tio(to_run, time_dir):
  print "echo", to_run, ">", out_parsed + "_seq-wr"
  print "echo", to_run, ">", out_parsed + "_rnd-wr"
  print "echo", to_run, ">", out_parsed + "_seq-rd"
  print "echo", to_run, ">", out_parsed + "_rnd-rd"

  for nb_threads in threads:
    print "echo -n > tmp" # an empty file tmp
    print "echo", to_run + str(nb_threads), ">>", out_all
    for i in range(nb_iterations):
      print "{ ", to_run + str(nb_threads), "; } >> tmp"
    print "cat tmp >>", out_all

#    print "echo -ne", nb_threads, "'\t'", ">>", out_parsed + "_seq-wr"
    print "grep 'Seq Write' tmp |"
    print "awk '{ rate += $7; } END {print rate/NR;}'",
    print ">>", out_parsed + "_seq-wr"
 
#    print "echo -ne", nb_threads, "'\t'", ">>", out_parsed + "_rnd-wr"
    print "grep 'Rnd Write' tmp |"
    print "awk '{ rate += $7; } END {print rate/NR;}'",
    print ">>", out_parsed + "_rnd-wr"

#    print "echo -ne", nb_threads, "'\t'", ">>", out_parsed + "_seq-rd"
    print "grep 'Seq Read' tmp |"
    print "awk '{ rate += $7; } END {print rate/NR;}'",
    print ">>", out_parsed + "_seq-rd"

#    print "echo -ne", nb_threads, "'\t'", ">>", out_parsed + "_rnd-rd"
    print "grep 'Rnd Read' tmp |"
    print "awk '{ rate += $7; } END {print rate/NR;}'",
    print ">>", out_parsed + "_rnd-rd"

  # the files created for measuring time (date_time_all and date_time_parsed) will be in time_dir
  print "mv", out_all, time_dir
  print "mv", out_parsed + "_seq-wr", out_parsed + "_rnd-wr", time_dir
  print "mv", out_parsed + "_seq-rd", out_parsed + "_rnd-rd", time_dir
  print "rm tmp"


def measure_time(name, to_run, time_dir):
  print "echo", to_run, ">", out_parsed + "_" + name

  for nb_threads in threads:
    print "echo -n > tmp" # an empty file tmp
    print "echo", to_run + str(nb_threads), ">>", out_all + "_" + name
    for i in range(nb_iterations):
      print "{ time", to_run + str(nb_threads), "; } 2>> tmp"
    print "cat tmp >>", out_all + "_" + name
#    print "echo -ne", nb_threads, "'\t'", ">>", out_parsed
 
    # take minutes and seconds as substrings and calculate time only in seconds
    print "grep real tmp |",
    print "awk '{ " + \
               "im = index($2,\"m\");" + \
               "is = index($2,\"s\");" + \
               "m  = substr($2,1,im-1);" + \
               "s  = substr($2,im+1,is-im-1);" + \
               "t += m*60+s } END {print t/NR;}'",
    print ">>", out_parsed + "_" + name

# the files created for measuring time (date_time_all and date_time_parsed) will be in time_dir
  print "mv", out_all + "_" + name, out_parsed + "_" + name, time_dir
#  print "rm tmp"


############ compile TM-dietlibc and TioBench (deterministic) ##############
print "make clean"
if run_tiotest == 1:
  print "make tiotest DETRANS=1"
  print "cd ./test/tiobench/"
  print "mv tiotest tiotest-det"
else:
  print "make micro_all DETRANS=1"
  print "cd ./test/microbench"
  print "mv micro_FGETC micro_FGETC-det"
  print "mv micro_FGETS micro_FGETS-det"
  print "mv micro_FREAD micro_FREAD-det"
  print "mv micro_FPUTC micro_FPUTC-det"
  print "mv micro_FPUTS micro_FPUTS-det"

############ run TioBench (deterministic)  ##################################

for name in benchmarks:
  print "if [ ! -d", time_nondet_dir, "]; then"
  print "mkdir", time_nondet_dir
  print "fi"
  print "if [ ! -d", time_det_dir, "]; then"
  print "mkdir", time_det_dir
  print "fi"

  # run deterministically
  to_run = "./" + name + "-det " + benchmarks[name]

  if run_tiotest == 1:
    measure_time_tio(to_run, time_det_dir)
  else:
    measure_time(name, to_run, time_det_dir)

######### compile TM-dietlibc and TioBench (nondeterministic) ###########
print "cd -"
print "make clean"
if run_tiotest == 1:
  print "make tiotest"
  print "cd ./test/tiobench/"
  print "mv tiotest tiotest-nondet"
else:
  print "make micro_all"
  print "cd ./test/microbench"
  print "mv micro_FGETC micro_FGETC-nondet"
  print "mv micro_FGETS micro_FGETS-nondet"
  print "mv micro_FREAD micro_FREAD-nondet"
  print "mv micro_FPUTC micro_FPUTC-nondet"
  print "mv micro_FPUTS micro_FPUTS-nondet"


############ run TioBench (nondeterministic) #############################
for name in benchmarks:
  print "if [ ! -d", time_nondet_dir, "]; then"
  print "mkdir", time_nondet_dir
  print "fi"
  print "if [ ! -d", time_det_dir, "]; then"
  print "mkdir", time_det_dir
  print "fi"

  # run nondeterministically
  to_run = "./" + name + "-nondet " + benchmarks[name]

  if run_tiotest == 1:
    measure_time_tio(to_run, time_nondet_dir)
  else:
    measure_time(name, to_run, time_nondet_dir)
