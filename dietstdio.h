/* diet stdio */

#include <sys/cdefs.h>
#include <sys/types.h>
#include "dietfeatures.h"
#ifdef WANT_THREAD_SAFE
#include <pthread.h>
#endif
#include <stdarg.h>

#ifdef WANT_SMALL_STDIO_BUFS
#define BUFSIZE 128
#else
#define BUFSIZE 2048
#endif

struct __stdio_file {
  int fd;
//  char pad1[64];
  int flags;
//  char pad2[64];
  uint32_t bs;	/* read: bytes in buffer */
//  char pad3[64];
  uint32_t bm;	/* position in buffer */
//  char pad4[64];
  uint32_t buflen;	/* length of buf */
//  char pad5[64];
  char *buf;
//  char pad6[64];
  struct __stdio_file *next;	/* for fflush */
//  char pad7[64];
  pid_t popen_kludge;
//  char pad8[64];
  unsigned char ungetbuf;
//  char pad9[64];
  char ungotten;
//  char pad10[64];
  int sys_position; /* tm-dietlibc change: copy of os file position */
//  char pad11[64];
//  char was_modified; /* indicates if buffer was modified */
//  char pad12[64];
  char is_terminal;
//  char pad13[64];

#ifdef WANT_THREAD_SAFE
  pthread_mutex_t m;
#endif
};

#define ERRORINDICATOR 1
#define EOFINDICATOR 2
#define BUFINPUT 4
#define BUFLINEWISE 8
#define NOBUF 16
#define STATICBUF 32
#define FDPIPE 64
#define CANREAD 128
#define CANWRITE 256
#define CHECKLINEWISE 512

#define _IONBF 0
#define _IOLBF 1
#define _IOFBF 2

#include <stdio.h>

/* internal function to flush buffer.
 * However, if next is BUFINPUT and the buffer is an input buffer, it
 * will not be flushed. Vice versa for output */
extern int __fflush4(FILE *stream,int next);
 __attribute__ ((transaction_safe))
extern int tx___fflush4(FILE *stream,int next);
extern int __buffered_outs(const char *s,size_t len);

/* ..scanf */
struct arg_scanf {
  void *data;
  int (*getch)(void*);
  int (*putch)(int,void*);
};

int __v_scanf(struct arg_scanf* fn, const char *format, va_list arg_ptr);

struct arg_printf {
  void *data;
  int (*put)(void*,size_t,void*);
};

int __v_printf(struct arg_printf* fn, const char *format, va_list arg_ptr);
int __v_printf_unlocked(FILE *stream, const char *format, va_list arg_ptr);
int __v_printf_lock(FILE *stream, const char *format, va_list arg_ptr);

extern FILE *__stdio_root;

int fflush_unlocked(FILE *stream);
int tx_fflush_unlocked(FILE *stream);
__attribute__ ((transaction_safe))
int __fflush_stdin(void);
__attribute__ ((transaction_safe))
int __fflush_stdout(void);
__attribute__ ((transaction_safe))
int __fflush_stderr(void);

FILE* __stdio_init_file_nothreads(int fd,int closeonerror,int mode);
__attribute__ ((transaction_pure))
int __stdio_parse_mode(const char *mode) __attribute__((__pure__));
void __stdio_flushall(void);
void __stdio_flushall_lock(void);

#ifndef __THREAD_INTERNAL_H__
__attribute__ ((transaction_pure))
int __libc_close(int fd);
__attribute__ ((transaction_pure))
int __libc_open(const char*fn,int flags,...);
 __attribute__ ((transaction_pure))
ssize_t __libc_read(int fd,void*buf,size_t len);
 __attribute__ ((transaction_pure))
ssize_t __libc_write(int fd,const void*buf,size_t len);
#endif

void munmap_deferral(void* addr);

int __stdout_is_tty(void);
__attribute__ ((transaction_pure))
int __stdin_is_tty(void);
