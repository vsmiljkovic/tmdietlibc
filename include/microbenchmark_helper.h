#include <errno.h>
#include <dietstdio.h>
#include <endian.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <pthread.h>
#include <dietlibc-tanger-stm.h>

#ifndef RAND_OFFSET_MODE

#define SEQ_OFFSET_MODE 0
#define RAND_OFFSET_MODE 1

#define FGETC_FUNCTION_MODE 0
#define FGETS_FUNCTION_MODE 1
#define FREAD_FUNCTION_MODE 2
#define FSCANF_FUNCTION_MODE 3
#define FSEEK_FUNCTION_MODE 4

#define FGETC_FPUTC_FUNCTION_MODE 5
#define FGETS_FPUTS_FUNCTION_MODE 6
#define FREAD_FWRITE_FUNCTION_MODE 7
#define FSCANF_FPRINTF_FUNCTION_MODE 8

#define NUM_OF_READS_IN_LOOP 4
#define NOLOCKS_CONCURR_MODE 0
#define LOCKS_CONCURR_MODE 1
#define TXUNDO_CONCURR_MODE 2
#define TXIRR_CONCURR_MODE 3
#endif


int last_char0, last_char1, last_char2, last_char3, last_char4, last_char5, last_char6, last_char7;
char* last_string;
int last_fseek;
long sum_chars;
long num_of_iterations_per_thread;
long num_of_iterations_for_read;
unsigned offset_mode;
unsigned offset_coeff;
unsigned concurr_mode;
unsigned num_read_chars;
char fscanf_param[10];

/* ################################################################### *
 * BARRIER
 * ################################################################### */

typedef struct barrier {
    pthread_cond_t complete;
    pthread_mutex_t mutex;
    int count;
    int crossing;
} barrier_t;

barrier_t barrier;

void barrier_init(barrier_t *b, int n);
void barrier_cross(barrier_t *b);

///////////////////////////////////////////////////////////////////////////////////////


typedef struct thread_data {
  FILE* file;
  unsigned int seed;
  unsigned int num_of_char_read;
}thread_data_t;

int get_offset_0(int offset, unsigned int* seed);
int get_offset_4(int offset, unsigned int* seed);
int get_offset_8(int offset, unsigned int* seed);
int get_offset_N(int offset, unsigned int* seed, unsigned N);

void *thread_work_4_fgetc(void *arg);
void *thread_work_4_fscanf(void *arg);
void *thread_work_only_fseek(void *arg);

void *thread_work_X_fgets(void *arg);
void *thread_work_X_fread(void *arg);
void *thread_work_X_fscanf(void *arg);

void *thread_work_X_fgetc_fputc(void *arg);
void *thread_work_X_fgets_fputs(void *arg);
void *thread_work_X_fread_fwrite(void *arg);
void *thread_work_X_fscanf_fprintf(void *arg);
