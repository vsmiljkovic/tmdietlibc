#ifndef DETRANSH
#define DETRANSH 1

#include <syscalls.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <assert.h>
#include <time.h>
#include <stm.h>

#include <fcntl.h>
#include <linux/perf_event.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include <asm/unistd.h>
#include <sys/mman.h>

pthread_t first_tid;

#define TICKET_LOCK 1
#ifdef TICKET_LOCK

typedef unsigned char u8;
typedef unsigned short u16;

typedef u8  __ticket_t;
typedef u16 __ticketpair_t;

typedef struct arch_spinlock {
  union {
    __ticketpair_t head_tail;
    struct __raw_tickets {
      __ticket_t head, tail;
    } tickets;
  };
} arch_spinlock_t;

arch_spinlock_t* det_lock;

void ticket_spin_lock(arch_spinlock_t*);
void ticket_spin_unlock(arch_spinlock_t*);
void ticket_spin_init(arch_spinlock_t**);
void ticket_spin_destroy(arch_spinlock_t*);

#define DET_LOCK(x) ticket_spin_lock(x)
#define DET_UNLOCK(x) ticket_spin_unlock(x)
#define DET_LOCK_INIT(x) ticket_spin_init(&x)
#define DET_LOCK_DESTROY(x) ticket_spin_destroy(x)
#define YIELD() /* nothing */

#elif defined(SPIN_LOCK)

#define lock_t pthread_spinlock_t
#define DET_LOCK(x) pthread_spin_lock(&x)
#define DET_UNLOCK(x) pthread_spin_unlock(&x)
#define DET_LOCK_INIT(x) pthread_spin_init(&x, 0)
#define DET_LOCK_DESTROY(x) /* nothing */
#define YIELD() pthread_yield()

#elif defined(MUTEX)

#define lock_t pthread_mutex_t
#define DET_LOCK(x) pthread_mutex_lock(&x)
#define DET_UNLOCK(x) pthread_mutex_unlock(&x)
#define DET_LOCK_INIT(x) pthread_mutex_init(&x, 0)
#define DET_LOCK_DESTROY(x) /* nothing */
#define YIELD() /* nothing */

#endif

int deterministic;
int irrevocable;
pthread_t irr_tid;

#define MAX_NUM_THREADS 10 // we support 9 threads (main thread + 8 threads),
                           // but because of % operation, it's 9+1=10
#define MAX_NUM_BARRIERS 2 // currently we support only 1 barrier,
                           // but because of % operation, it's 1+1=2
typedef struct list_t {
  pthread_t list[MAX_NUM_THREADS]; 
  unsigned int size;
  unsigned int first;
} list_t;

typedef struct hash_pthread_t {
  pthread_t threads[MAX_NUM_THREADS];
  list_t blocked[MAX_NUM_THREADS];
  unsigned int size;
  unsigned int first;
} hash_pthread_t;

typedef struct hash_barrier_t {
  pthread_barrier_t* barr[MAX_NUM_BARRIERS];
  list_t threads[MAX_NUM_BARRIERS];
  unsigned int size;  // current size 
  unsigned int first; // current first
  unsigned int total_size[MAX_NUM_BARRIERS];
} hash_barrier_t;


/*static unique_ptr<list<pthread_t> > ready_list;
static list<pthread_t> zombies;
static unordered_map<pthread_t, vector<pthread_t> > blocked;
static unique_ptr<list<pthread_t> > blocked_txns;
static unordered_map<pthread_barrier_t*, vector<pthread_t> > barriers;
static unordered_map<pthread_barrier_t*, unsigned> barriers_size;*/

list_t ready_threads;
list_t zombies;
hash_pthread_t blocked_threads;
list_t blocked_txns;
hash_barrier_t barriers;

#ifdef ADHOC_SUPPORT
float ratio_with_err;
#endif

//#define DEBUG2 1
#ifdef DEBUG
__attribute__((transaction_pure))
int fprintf(FILE *stream, const char *format, ...);
#define DEBUG_PRINT(...) do{ } while (0)
#define DEBUG_PRINT2(...) do{ } while ( 0 )
#define DEBUG_PRINT_STATE() /* nothing */
#define ASSERT(x) assert(x)

#elif defined DEBUG2
__attribute__((transaction_pure))
int fprintf(FILE *stream, const char *format, ...);
#define DEBUG_PRINT(...) do{ fprintf_lock( stderr, __VA_ARGS__ ); } while( 0 )
#define DEBUG_PRINT2(...) do{ fprintf_lock( stderr, __VA_ARGS__ ); } while( 0 )
#define DEBUG_PRINT_STATE() print_state("");
#define ASSERT(x) assert(x)

#else /* !DEBUG */
#define DEBUG_PRINT(...) do{ } while ( 0 )
#define DEBUG_PRINT2(...) do{ } while ( 0 )
#define DEBUG_PRINT_STATE() /* nothing */
#define ASSERT(x) /* assert(x) nothing */

#endif

#define WAIT_TURN 0
#define CONTINUE_IRR 1
#define ROLLBACK 2

typedef struct my_pair {
  void*arg;
  void*(*f)(void*);
  pthread_t main_tid;
} my_pair;

void detrans_init();
void detrans_exit();

#ifdef ADHOC_SUPPORT
#ifndef SAMPLE_PERIOD
//#warning "SAMPLE_PERIOD 10000"
#define SAMPLE_PERIOD 10000
//#else
//#warning "SAMPLE_PERIOD defined"
#endif
#ifndef RATIO
//#warning "RATIO 4"
#define RATIO 4
//#else
//#warning "RATIO defined"
#endif
#endif

__attribute__((transaction_pure))
void give_turn_no_disable();

__attribute__((transaction_pure))
void give_turn();

__attribute__((transaction_pure))
void wait_turn_no_enable();

__attribute__((transaction_pure))
int wait_turn_or_irr(pthread_t id);

__attribute__((transaction_pure))
void push_back_from_front();

void print_state(char* init_text);
void* helper(void* arg);
void blockedTxnPushBack(pthread_t t);
void blockedThreadPushBack(pthread_t t, pthread_t ti);
unsigned int getBlockedSize(pthread_t t);
void blockedPopBack(pthread_t t);
void zombiesPushBack(pthread_t t);
void readyPushFront(pthread_t t);
unsigned int barrierPushBack(pthread_barrier_t *barrier, pthread_t ti);

my_pair* pre_pthread_create(void *(*start_routine)(void*), void *arg);
void post_pthread_create(pthread_t *thread);
int pre_pthread_join(pthread_t t);
void post_pthread_join(int hasZombies);
void pre_pthread_barrier_init(pthread_barrier_t* b, int n);
void pre_pthread_barrier_wait(pthread_barrier_t *barrier);
void post_pthread_barrier_wait();

#ifdef ADHOC_SUPPORT
void perf_event_init();
void perf_event_close();
#endif // ADHOC_SUPPORT
void perf_event_enable();
void perf_event_enable();
int is_perf_event_enabled();
#endif
