#ifndef _DIETLIBC_TM_H_
#define _DIETLIBC_TM_H_

#ifndef USE_LOCK
// #include <tanger-stm.h>
// #include <tanger-stm-internal.h>
// #include <tanger-stm-std-string.h>
// #include <tanger-stm-std-math.h>
// #include <tanger-stm-std-stdarg.h>
#include <libitm.h>
#include <detrans.h>
#endif

short commit_var;

#ifdef USE_TM
  #define ATOMIC_BEGIN(lock) TRANSACTION_BEGIN
#elif defined USE_TMIRR
  #define ATOMIC_BEGIN(lock) TRANSACTION_BEGIN SET_IRREVOCABILITY
#elif defined USE_LOCK
  #define ATOMIC_BEGIN(lock) \
    if (lock != NULL) { pthread_mutex_lock(lock); } \
    else { printf("No locks for synchronization\n"); }
#else
  #warning "Synchronization mechanism not set"
#endif

/* Atomic block that will always be executed serial. */
#if defined USE_TM || defined USE_TMIRR
  #define ATOMIC_BEGIN_SER(lock) TRANSACTION_BEGIN SET_IRREVOCABILITY
#elif defined USE_LOCK
  #define ATOMIC_BEGIN_SER(lock) \
    if (lock != NULL) { pthread_mutex_lock(lock); } \
    else { printf("No locks for synchronization\n"); }
#else
  #warning "Synchronization mechanism not set"
#endif

#ifdef USE_TM
  #define ATOMIC_END(lock) TRANSACTION_END
#elif defined USE_TMIRR
  #define ATOMIC_END(lock) TRANSACTION_END
#elif defined USE_LOCK
  #define ATOMIC_END(lock) \
     if (lock != NULL) { pthread_mutex_unlock(lock); } \
     else { printf("No locks for synchronization\n"); }
#else
  #warning "Synchronization mechanism not set"
#endif

//typedef void (* _ITM_userUndoFunction)(void *);

//extern int _ITM_getTransactionId() __attribute__ ((transaction_pure));
//extern void _ITM_addUserUndoAction(_ITM_userUndoFunction, void *) __attribute__ ((transaction_pure));
//extern void _ITM_addUserCommitAction(_ITM_userUndoFunction, int, void*) __attribute__((transaction_pure));
#ifdef I386
 extern void _ITM_changeTransactionMode(modeSerialIrrevocable) __attribute__((transaction_pure)) __attribute__((regparm(2)));
#else
 extern void _ITM_changeTransactionMode(modeSerialIrrevocable) __attribute__((transaction_pure));
#endif
extern void _ITM_finalizeThread(void) __attribute__((transaction_pure));


#define SET_IRREVOCABILITY _ITM_changeTransactionMode(modeSerialIrrevocable, NULL);
//#ifdef I386
//#define SET_SYSCALLSAFE
//#else
//#define SET_SYSCALLSAFE _ITM_changeTransactionMode(modeSyscallSafe);
//#endif
#define REGISTER_UNDO(f, p) _ITM_addUserUndoAction(f, p);
#define REGISTER_DEFERRAL(f, p) _ITM_addUserCommitAction(f, _ITM_getTransactionId(), p);
#define TM_INIT _ITM_initializeProcess();
#define TM_SHUTDOWN _ITM_finalizeProcess();
#define TM_THREAD_INIT 
#define TM_THREAD_SHUTDOWN _ITM_finalizeThread();
#ifdef ADHOC_SUPPORT
#define TRANSACTION_BEGIN perf_event_disable(); __transaction_atomic {
#define TRANSACTION_END } perf_event_enable();
#else
#define TRANSACTION_BEGIN __transaction_atomic {
#define TRANSACTION_END }
#endif
#endif /* _DIETLIBC_TM_H_ */

