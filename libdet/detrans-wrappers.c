#include "detrans.h"

__thread pthread_t tid;
__thread pthread_t main_tid;

/*
 * Use it instead of debugging. DEBUG2 has to be defined.
 */
void print_state(char* init_text) {
  unsigned int i, j, last, last_th;  
 
  DEBUG_PRINT2("%s\n", init_text);

  DEBUG_PRINT2("ready threads: [ ");
  last = get_last(ready_threads.first, ready_threads.size);
  for (i = ready_threads.first; i != last; i = (i + 1) % MAX_NUM_THREADS) {
    DEBUG_PRINT2("%ld ", ready_threads.list[i] - first_tid);
  }
  
  DEBUG_PRINT2("]\nzombies: [ ");
  last = get_last(zombies.first, zombies.size);
  for (i = zombies.first; i != last; i = (i + 1) % MAX_NUM_THREADS) {
    DEBUG_PRINT2("%ld ", zombies.list[i] - first_tid);
  }

  DEBUG_PRINT2("]\nbarriers: [ ");
  last = get_last_barr(barriers.first, barriers.size);
  for (i = barriers.first; i != last; i = (i + 1) % MAX_NUM_BARRIERS) {
    DEBUG_PRINT2("%ld (", barriers.barr[i]);
    list_t tids = barriers.threads[i];
    last_th = get_last(tids.first, tids.size);
    for (j = tids.first; j != last_th; j = (j + 1) % MAX_NUM_THREADS) {
      DEBUG_PRINT2("%ld ", tids.list[j] - first_tid);
    }
    DEBUG_PRINT2(") ");
  }
  
  DEBUG_PRINT2("]\nblocked_txns: [ ");
  last = get_last(blocked_txns.first, blocked_txns.size);
  for (i = blocked_txns.first; i != last; i = (i + 1) % MAX_NUM_THREADS) {
    DEBUG_PRINT2("%ld ", blocked_txns.list[i] - first_tid);
  }
  DEBUG_PRINT2("]\n");
}

/*
 * The same as print_state, except DEBUG2 does not have to be defined. 
 */
void print_always() {
  unsigned int i, j, last, last_th;
 
  fprintf_lock(stderr, "ready threads: [ ");
  last = get_last(ready_threads.first, ready_threads.size);
  for (i = ready_threads.first; i != last; i = (i + 1) % MAX_NUM_THREADS) {
    fprintf_lock(stderr, "%ld ", ready_threads.list[i]);
  }
 
  fprintf_lock(stderr, "]\nzombies: [ ");
  last = get_last(zombies.first, zombies.size);
  for (i = zombies.first; i != last; i = (i + 1) % MAX_NUM_THREADS) {
    fprintf_lock(stderr, "%ld ", zombies.list[i]);
  }
 
  fprintf_lock(stderr, "]\nblocked threads: [ ");
  last = get_last(blocked_threads.first, blocked_threads.size);
  for (i = blocked_threads.first; i != last; i = (i + 1) % MAX_NUM_THREADS) {
    fprintf_lock(stderr, "%ld (", blocked_threads.threads[i]);
    list_t tids = blocked_threads.blocked[i];
    last_th = get_last(tids.first, tids.size);
    for (j = tids.first; j != last_th; j = (j + 1) % MAX_NUM_THREADS) {
      fprintf_lock(stderr, "%ld ", tids.list[j]);
    }
    fprintf_lock(stderr, ") ");
  }
 
  fprintf_lock(stderr, "]\nbarriers: [ ");
  last = get_last_barr(barriers.first, barriers.size);
  for (i = barriers.first; i != last; i = (i + 1) % MAX_NUM_BARRIERS) {
    fprintf_lock(stderr, "%ld (", barriers.barr[i]);
    list_t tids = barriers.threads[i];
    last_th = get_last(tids.first, tids.size);
    for (j = tids.first; j != last_th; j = (j + 1) % MAX_NUM_THREADS) {
      fprintf_lock(stderr, "%ld ", tids.list[j]);
    }
    fprintf_lock(stderr, ") ");
  }
 
  fprintf_lock(stderr, "]\nblocked_txns: [ ");
  last = get_last(blocked_txns.first, blocked_txns.size);
  for (i = blocked_txns.first; i != last; i = (i + 1) % MAX_NUM_THREADS) {
    fprintf_lock(stderr, "%ld ", blocked_txns.list[i]);
  }
  fprintf_lock(stderr, "]\n");
}

int pthread_yield() {
  readyPushBack(getReadyFront());
  give_turn_no_disable();
  wait_turn_no_enable();
  return 0;
}

void* helper(void* arg) {
  void* ret;
  int tmp, blocked_size;

  my_pair p = * (my_pair*) arg;

  if (deterministic) {
    tid = pthread_self();
    main_tid = p.main_tid;

#ifdef ADHOC_SUPPORT
  perf_event_init();
#endif

    /*tmp = wait_turn_or_irr(tid);
    ASSERT(tmp == 1); // it's my turn */
    wait_turn();  
    print_state("helper1");
  }
  ret = p.f(p.arg);

  if (deterministic) {

#ifdef ADHOC_SUPPORT
    perf_event_disable();
    perf_event_close();
#endif

    print_state("helper2");

    DEBUG_PRINT("thread exiting %ld\n", tid-first_tid);

    blocked_size = getBlockedSize(tid);
    if (blocked_size > 0) {
      // Vesna: the main thread is already waiting for me
      // blocked thread will join on our `tid'
      ASSERT(blocked_size == 1);
      blockedPopBack(tid);
      ASSERT(blocked_threads.size == 0 || getBlockedSize(tid) == 0);
      print_state("\nhelper - end");


      ASSERT(tid == getReadyFront());
      readyPopFront(); 
      if (isReadyEmpty()) {
        readyPushBack(main_tid);
      }

      DET_UNLOCK(det_lock);
    //  give_turn_no_disable();
    }
    else {
      // Vesna: nobody is waiting for me yet
      zombiesPushBack(tid);

      ASSERT(tid == getReadyFront());
      readyPopFront(); 
      if (isReadyEmpty()) {
        readyPushBack(main_tid);
      }
      DET_UNLOCK(det_lock);
    }
  }

  return ret;
}

my_pair* pre_pthread_create(void *(*start_routine)(void*), void *arg) {
  my_pair *p = (my_pair*)malloc(sizeof(my_pair));
  p->arg = arg;
  p->f = start_routine;
  p->main_tid = tid;

#ifdef ADHOC_SUPPORT
    perf_event_disable();
#endif

  return p;
}

void post_pthread_create(pthread_t *thread) {
  if (deterministic) {

#ifdef ADHOC_SUPPORT
  perf_event_enable();
#endif

    readyPushBack(*thread);
  }
}

int pre_pthread_join(pthread_t t) {
  int hasZombies = 0;

  if (deterministic) {
    unsigned int i, j, last, before_last;
    DEBUG_PRINT2("pre_pthread_join\n");

#ifdef ADHOC_SUPPORT
      perf_event_disable();
#endif

    last = get_last(zombies.first, zombies.size);
    before_last = get_last(zombies.first, zombies.size-1);
    for (i = zombies.first; i != last; i = (i + 1) % MAX_NUM_THREADS) {
      if (zombies.list[i] == t) {
        for (j = i; j != before_last; j = (j + 1) % MAX_NUM_THREADS) {
           zombies.list[j] = zombies.list[(j + 1) % MAX_NUM_THREADS]; //not good
        }
        zombies.size--;
        break;
      }
    }
    if (i != last) {
      hasZombies = 1;
      DEBUG_PRINT2("zombies erasing %lu\n", t-first_tid);
  //    zombies.first = zombies.size = 0;

    }
    else {
      DEBUG_PRINT2("blocking %ld\n", tid-first_tid);
      blockedThreadPushBack(t, tid);
      give_turn_no_disable();
    }
  }
  return hasZombies;
}

void post_pthread_join(int hasZombies) {
  if (deterministic) {
    DEBUG_PRINT2("post_pthread_join\n"); 
    if (!hasZombies) {
     // DET_LOCK(det_lock);
       wait_turn_no_enable();
//      assert(contains(*ready_list, t));
//      assert(ready_threads.list[ready_threads.first] == t);
 //     ASSERT(tid == getReadyFront());
 //     readyPopFront();
//      assert(!contains(*ready_list, tid));
 //     readyPushFront(tid);
 //     DEBUG_PRINT2("waiting %lu\n", t);
    }

#ifdef ADHOC_SUPPORT
    perf_event_enable();
#endif

  }
}

void pre_pthread_barrier_init(pthread_barrier_t* b, int n) {
  if (deterministic) {
    unsigned int last;
    ASSERT(barriers.size <= 1); // if it fails, increase MAX_NUM_BARRIERS
                             // and change barrier wrappers
    last = get_last_barr(barriers.first, barriers.size);
    barriers.barr[last] = b;
    barriers.size++;
    barriers.total_size[last] = n;
  }
}

void pre_pthread_barrier_wait(pthread_barrier_t *b) {
  if (deterministic) {
    unsigned int i, last;
    unsigned int index = barrierPushBack(b, tid);
    DEBUG_PRINT2("barr wait %ld\n", tid-first_tid);
    print_state("before barr_wait");
    if (barriers.threads[index].size == barriers.total_size[index]) {
      if (barriers.total_size[index] != 1) {
        list_t tids = barriers.threads[index];
        last = get_last(tids.first, tids.size);
        for (i = tids.first; i != last; i = (i + 1) % MAX_NUM_THREADS) {
          readyPushBack(tids.list[i]);
        }
      }
      barriers.threads[index].size = barriers.threads[index].first = 0;
      // don't remove the barrier, it might be used again - like in TioBench
      // barriers.size = barriers.first = 0;
    }
    print_state("after barr_wait");
    give_turn();
  }
}

void post_pthread_barrier_wait() {
  if (deterministic) {
    wait_turn();
  }
}

// TODO: call it from TM-dietlibc (not from benchmarks)
void detrans_init() {
#ifdef DETRANS
#warning "Compiling with DeTrans."
   fprintf_lock(stderr, "Running with DeTrans\n");
   deterministic = 1;
   irrevocable = 0;
   irr_tid = 0;
   DEBUG_PRINT2("detrans_init\n");
   DET_LOCK_INIT(det_lock);
   DET_LOCK(det_lock);
   tid = pthread_self();
   readyPushFront(tid);

   first_tid = tid;

#ifdef ADHOC_SUPPORT
  ratio_with_err = RATIO + RATIO * 0.05;
  set_sigaction();
  perf_event_init();
  perf_event_enable();
#endif

#else
#warning "Compiling without DeTrans."
   fprintf_lock(stderr, "Running without DeTrans\n");
   deterministic = 0;
#endif
}

// TODO: call it from TM-dietlibc (not from benchmarks)
void detrans_exit() {
#ifdef DETRANS

#ifdef ADHOC_SUPPORT
  perf_event_disable();
  perf_event_close();
#endif

  DET_UNLOCK(det_lock);
  DET_LOCK_DESTROY(det_lock);
  DEBUG_PRINT2("detrans_exit");
  DEBUG_PRINT_STATE();
#endif
}
