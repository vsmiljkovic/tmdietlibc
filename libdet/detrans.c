#include <stdio.h>
#include "detrans.h"

extern __thread pthread_t tid;
extern __thread pthread_t main_tid;

static __thread long start_count;
static __thread long commit_count;
static __thread unsigned txn_running;

#ifdef ADHOC_SUPPORT
static __thread int fd_ins;
static __thread int fd_br;
static __thread int enabled=0;
#ifdef PERF_RESET_HACK
static __thread long ins_prev=0;
static __thread long br_prev=0;
#endif

#define OVERFLOWS_TO_TRACK 256  
static __thread long overflow_counts[OVERFLOWS_TO_TRACK]; // has to be long in dietlibc! long long does not work well!!!
static __thread long br_counts[OVERFLOWS_TO_TRACK];       // has to be long in dietlibc! long long does not work well!!!
static __thread int overflows=0;
#endif

__attribute__((transaction_pure))
static long get_start_count() {
   return start_count;
}

__attribute__((transaction_pure))
static void inc_start_count() {
   start_count++;
}

__attribute__((transaction_pure))
long get_commit_count() {
   if (!deterministic) return;
   return commit_count;
}

__attribute__((transaction_pure))
static void inc_commit_count() {
   commit_count++;
}

int isReadyEmpty() {
  return !ready_threads.size;
}
 
pthread_t getReadyFront() {
  return ready_threads.list[ready_threads.first];
}

inline unsigned int get_last(unsigned int first, unsigned int size) {
  return (first + size) % MAX_NUM_THREADS;
}

inline unsigned int get_last_barr(unsigned int first, unsigned int size) {
  return (first + size) % MAX_NUM_BARRIERS;
}
 
void readyPopFront() {
  print_state("readyPopFront");
  ASSERT(ready_threads.size > 0);
  ready_threads.first = (ready_threads.first + 1) % MAX_NUM_THREADS;
  ready_threads.size--;
}

void readyPushFront(pthread_t t) {
  unsigned int index;
  ASSERT(ready_threads.size < MAX_NUM_THREADS);
  index = (ready_threads.first - 1 + MAX_NUM_THREADS) % MAX_NUM_THREADS;
  ready_threads.list[index] = t;
  ready_threads.first = index;
  ready_threads.size++;
}

void readyPushBack(pthread_t t) {
  unsigned int last;
  ASSERT(ready_threads.size < MAX_NUM_THREADS);
  last = get_last(ready_threads.first, ready_threads.size);
  ready_threads.list[last] = t;
  ready_threads.size++;
}

void blockedTxnPushBack(pthread_t t) {
  unsigned int last;
  ASSERT(blocked_txns.size < MAX_NUM_THREADS);
  last = get_last(blocked_txns.first, blocked_txns.size);
  blocked_txns.list[last] = t;
  blocked_txns.size++;
}

void blockedThreadPushBack(pthread_t t, pthread_t ti) {
  unsigned int i, last, last_bl;

  ASSERT(blocked_threads.size < MAX_NUM_THREADS);
  last = get_last(blocked_threads.first, blocked_threads.size);
  blocked_threads.threads[last] = t;
  blocked_threads.size++;


  last_bl = get_last(blocked_threads.blocked[last].first, blocked_threads.blocked[last].size);
  blocked_threads.blocked[last].list[last_bl] = ti;
  blocked_threads.blocked[last].size++;
}

unsigned int getBlockedSize(pthread_t t) {
  unsigned int i, ret = -1;
  unsigned int last = get_last(blocked_threads.first, blocked_threads.size);
  for (i = blocked_threads.first; i != last; i = (i + 1) % MAX_NUM_THREADS) {
    if (blocked_threads.threads[i] == t) {
       ret = blocked_threads.blocked[i].size;
       break;
    }
  }
  // -1 is OK as well. ASSERT(ret != -1);
  return ret;
}

void blockedPopBack(pthread_t t) {
  unsigned int i, index = -1;
  unsigned int last = get_last(blocked_threads.first, blocked_threads.size);
  for (i = blocked_threads.first; i != last; i = (i + 1) % MAX_NUM_THREADS) {
    if (blocked_threads.threads[i] == t) {
       index = i;
       break;
    }
  }
  ASSERT(index != -1);
  //blocked_threads.blocked[index].first--;  
  blocked_threads.blocked[index].size--;
  if (blocked_threads.blocked[index].size == 0) {
    blocked_threads.size--;
  }
  DEBUG_PRINT("blockedPopBack ends");
}

void zombiesPushBack(pthread_t t) {
  unsigned int last;
  ASSERT(zombies.size < MAX_NUM_THREADS);
  last = get_last(zombies.first, zombies.size);
  zombies.list[last] = t;
  zombies.size++;
}
unsigned int barrierPushBack(pthread_barrier_t *barrier, pthread_t ti) {
  unsigned int i, index;
  unsigned int last = get_last_barr(barriers.first, barriers.size);
  for (i = barriers.first; i != last; i = (i + 1) % MAX_NUM_BARRIERS) {
    if (barriers.barr[i] == barrier) {
      index = i;
      break;
    }
  }
  ASSERT(index != last);
  list_t tids = barriers.threads[index];
  ASSERT(tids.size < MAX_NUM_THREADS);
  last = get_last(tids.first, tids.size);
  barriers.threads[index].list[last] = ti;
  barriers.threads[index].size++;
  return index;  
}

__attribute__((transaction_pure))
void push_back_from_front() {
  readyPushBack(getReadyFront());
}

void give_turn() {

#ifdef ADHOC_SUPPORT
  perf_event_disable();
#endif
 
  give_turn_no_disable();
}

__attribute__((transaction_pure)) 
void give_turn_no_disable() {
  unsigned int i, last;

  if (!deterministic) return;

  DEBUG_PRINT("give turn - start tid=%ld\n", tid - first_tid);
  print_state("give turn");

  if (tid != getReadyFront()) {  // This is very very bad!!!!! FIX IT!!!
    ASSERT(tid == getReadyFront());
  }
  else {
    readyPopFront();
  }

  if (isReadyEmpty()) {
    last = get_last(blocked_txns.first, blocked_txns.size);
    for (i = blocked_txns.first; i != last; i = (i + 1) % MAX_NUM_THREADS) {
       pthread_t t = blocked_txns.list[i];
       readyPushBack(t);
       DEBUG_PRINT2("unblock txn %ld\n", t - first_tid);
    }
    blocked_txns.first = blocked_txns.size = 0; 
  }
  
  print_state("give turn - end \n");
  
  DET_UNLOCK(det_lock);
} 

void wait_turn() {
  wait_turn_no_enable();

#ifdef ADHOC_SUPPORT
  perf_event_enable();
#endif

}

void wait_turn_no_enable() {
  if (!deterministic) return;
 // wait until it's your turn
  while (1) {
    DET_LOCK(det_lock);
    ASSERT(!isReadyEmpty());
    if (tid == getReadyFront()) {
      DEBUG_PRINT2("wait_turn: tid %ld - it is my turn\n", tid - first_tid);
      break; // det_lock will be unlocked in give_turn
             // or stm_set_irrevocable
    }
    else {
      if (txn_running && irrevocable && irr_tid != tid) {
        stm_kill_self();
        break;
      }
    }
    DET_UNLOCK(det_lock);
  }
}

void set_irrevocable() {
  if (!deterministic) return;
  irrevocable = 1;
  irr_tid = tid;
}

void unset_irrevocable() {
  if (!deterministic) return;
  irrevocable = 0;
  irr_tid = 0;
}

void det_unlock() {
  if (!deterministic) return;
#ifdef SERIAL
  return;
#endif
  DET_UNLOCK(det_lock);
}

void wait_irr_ended() {
  if (!deterministic) return;
#ifdef SERIAL
  return;
#endif
  while (1) {
    DET_LOCK(det_lock);
    if (!irrevocable) break;
    DET_UNLOCK(det_lock);
  }
  DEBUG_PRINT2("irr ended: tid %ld\n", tid - first_tid);
  DET_UNLOCK(det_lock);
}

void pre_stm_start() {
  int tmp;
  if (!deterministic) return;
#ifdef SERIAL
  return;
#endif
  DEBUG_PRINT2("pre_stm_start tid %ld\n", tid - first_tid);
  ASSERT(start_count == 0);
  ASSERT(commit_count == 0);
  ASSERT(txn_running == 0);
  if (get_start_count() == 0) {
    blockedTxnPushBack(tid);
    give_turn_no_disable();
    wait_turn_no_enable();
    DEBUG_PRINT2("stm_start() tid %ld\n", tid - first_tid);
    print_state("");
    push_back_from_front();
    give_turn_no_disable();
  }
  txn_running = 1;
  inc_start_count();
}

void post_stm_start() {
  if (!deterministic) return;
}

void pre_stm_commit() {
  if (!deterministic) return;
#ifdef SERIAL
  return;
#endif
  DEBUG_PRINT2("pre_stm_commit() tid %ld\n", tid - first_tid);
  if (get_commit_count() == 0) {
      wait_turn_no_enable();
      // it is my turn
      inc_commit_count();
      DEBUG_PRINT2("pre_stm_commit() - my turn - tid %ld\n", tid - first_tid);
      return;
  }
  else if (irrevocable && irr_tid == tid) {
    wait_turn_no_enable();
  }
  return; // it's my turn from before
}
 
void post_stm_commit(int ret) {
  if (!deterministic) return;
#ifdef SERIAL
  return;
#endif
  DEBUG_PRINT("post_stm_commit: tid %ld\n", tid - first_tid);
  start_count = 0;
  commit_count = 0;
  txn_running = 0;
  if (irr_tid == tid) {
    irrevocable = 0;
    irr_tid = 0;
  }
  ASSERT(ret);
  pthread_yield();
}

#ifdef ADHOC_SUPPORT
static void perf_event_handler(int signum, siginfo_t* info, void* ucontext) {
  int fd = info->si_fd;
  long ins, br;
  float ratio;
  int read_result;

#ifdef PERF_RESET_HACK
  long tmpi, tmpb;
#endif
 
  ASSERT(tid == getReadyFront());

  if (!enabled) {
    return;
  }

  if (fd == fd_ins) {
    /* Turn off measurement */
    perf_event_disable();

    /* Read out value */
    read_result=read(fd_ins,&ins,sizeof(long));
    assert(read_result == sizeof(ins));
    read_result=read(fd_br,&br,sizeof(long));
    assert(read_result == sizeof(br));

#ifdef PERF_RESET_HACK
    tmpi = ins;
    tmpb = br;

    ins -= ins_prev;
    br -= br_prev;

    ins_prev = tmpi;
    br_prev = tmpb;
#endif

    overflow_counts[overflows] = ins;
    br_counts[overflows] = br;

    /* Increment, but make sure we don't overflow */
    overflows++;
    if (overflows>=OVERFLOWS_TO_TRACK) {
      overflows=OVERFLOWS_TO_TRACK-1;
    }

//    if (overflows==5) {
//      ret=ioctl(fd_ins, PERF_EVENT_IOC_PERIOD,&sample_period_new);
//      if (ret<0) {}
//    }

    ratio = 1.0f * ins / br;

 //   fprintf(stderr, "fd = %d, overflows = %ld, ratio = %f, ratio_with_error = %f\n", fd, overflows, ratio, ratio_with_err);

    if (ratio < ratio_with_err) {
      DEBUG_PRINT2(stderr, "Token passed\n");
      push_back_from_front();
      give_turn_no_disable();
      wait_turn_no_enable();
    }
    perf_event_enable();
  }
  else {
    fprintf(stderr, "You shouldn't be here!\n");
  }
  (void)read_result; 
}

void set_sigaction() {
  struct sigaction sa;

  /* Set up overflow */
  memset(&sa, 0, sizeof(struct sigaction));
  sa.sa_sigaction = perf_event_handler;
  sa.sa_flags = SA_SIGINFO;
  if (sigaction( SIGIO, &sa, NULL) < 0) {
    printf("Error setting up signal handler\n");
  }
}

void perf_event_init() {
  long sp = SAMPLE_PERIOD;
  void *blargh;
  struct perf_event_attr pe, pe2;

  memset(&pe,0,sizeof(struct perf_event_attr));

  pe.type=PERF_TYPE_RAW;
  pe.config=(0x00C0ULL) | (0x0000ULL); //instructions retired
  pe.size=sizeof(struct perf_event_attr);
  pe.disabled=1;
  pe.pinned=1;
  pe.exclude_kernel=1;
  pe.exclude_hv=1;
  pe.sample_period=sp;
  pe.sample_type=PERF_SAMPLE_IP;
  pe.wakeup_events=1;
  fd_ins=perf_event_open(&pe,0,-1,-1,0);
  
  memset(&pe2,0,sizeof(struct perf_event_attr));

  pe2.type=PERF_TYPE_RAW;
  pe2.config=(0x00C4ULL) | (0x0100ULL);  // branches retired
  pe2.size=sizeof(struct perf_event_attr);
  pe2.disabled=0;
  pe2.exclude_kernel=1;
  pe2.exclude_hv=1;
  pe2.sample_period=sp;
  pe2.wakeup_events=1;
  fd_br=perf_event_open(&pe2,0,-1,fd_ins,0);

  blargh=mmap(NULL, (1+1)*4096, 
         PROT_READ|PROT_WRITE, MAP_PRIVATE, fd_ins, 0);

  fcntl(fd_ins, F_SETFL, O_RDWR|O_NONBLOCK|O_ASYNC);
  fcntl(fd_ins, F_SETSIG, SIGIO);
  fcntl(fd_ins, F_SETOWN, gettid());

//  fprintf(stderr, "event_init %d %d\n", fd_ins, fd_br, SAMPLE_PERIOD);
}

int is_perf_event_enabled() {
  return enabled;
}

void perf_event_enable() {
  int ret;
#ifndef PERF_RESET_HACK
  ret = ioctl(fd_ins, PERF_EVENT_IOC_RESET, 0);
  if (ret<0) {
    fprintf(stderr, "Error in RESET\n");
  }
  ret = ioctl(fd_br, PERF_EVENT_IOC_RESET, 0);
  if (ret<0) {
    fprintf(stderr, "Error in RESET\n");
  }
#endif
  ioctl(fd_ins, PERF_EVENT_IOC_ENABLE, 0);
  enabled = 1;
}

void perf_event_disable() {
  int ret;
  enabled = 0;
  ret = ioctl(fd_ins, PERF_EVENT_IOC_DISABLE, 0);
  if (ret<0) {
    fprintf(stderr, "Error in DISABLE\n");
  }
}

void perf_event_close() {
  long ins, br;
  int i;

  int read_result=read(fd_ins,&ins,sizeof(long));
  if (read_result != sizeof(long)) {
    fprintf(stderr, "read not working\n");
    exit(1);
  }
  
  read_result=read(fd_br,&br,sizeof(long));
  if (read_result != sizeof(long)) {
    fprintf(stderr, "read not working\n");
    exit(1);
  }

  close(fd_br);
  close(fd_ins);

/*  fprintf_lock(stderr, "Overflows, %d %d\n", fd_ins, fd_br);
  for(i=0;i<overflows;i++) {
    fprintf_lock(stderr, "\t%d %ld %ld\n",i,overflow_counts[i], br_counts[i]);
  }

  fprintf_lock(stderr, "\t %ld %ld\n", ins, br);*/
}
#else // !ADHOC_SUPPORT
void perf_event_enable() {}
void perf_event_disable() {}
#endif // ADHOC_SUPPORT
