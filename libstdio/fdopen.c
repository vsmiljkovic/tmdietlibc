#include <errno.h>
#include "dietfeatures.h"
#include <sys/types.h>
#include <dietstdio.h>
#include <unistd.h>

FILE *fdopen_unlocked(int filedes, const char *mode) {
  int f=0;	/* O_RDONLY, O_WRONLY or O_RDWR */
  FILE *stream;

  f=__stdio_parse_mode(mode);
  if (filedes<0) { errno=EBADF; return 0; }
  stream = __stdio_init_file_nothreads(filedes,0,f);
  stream->sys_position = lseek(stream->fd,0,SEEK_CUR);  // tm-change: file position update 
  return stream;
}

//FILE *fdopen(int filedes, const char *mode) __attribute__((weak,alias("fdopen_unlocked")));
