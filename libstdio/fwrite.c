#include <sys/types.h>
#include <stdio.h>
#include "dietstdio.h"
#include <unistd.h>
#include <errno.h>
#include <dietlibc-tanger-stm.h>

#ifdef USE_LOCK
int __fflush4(FILE *stream,int next) __attribute__ ((transaction_pure));
int fputc_unlocked(int c, FILE *stream) __attribute__ ((transaction_pure));
#endif

size_t fwrite_unlocked(const void *ptr, size_t size, size_t nmemb, FILE *stream) {
  ssize_t res;
  size_t len=size*nmemb;
  size_t i,done;
  char *src, *dst;
  
  if (stream->is_terminal) { /* TM-dietlibc */
#if defined USE_TM || defined USE_TMIRR
//    SET_SYSCALLSAFE;
    SET_IRREVOCABILITY;
#endif
  }

#if defined USE_TM || defined USE_TMIRR
  if (!__likely(stream->flags&CANWRITE) || tx___fflush4(stream,0)) {
#else
  if (!__likely(stream->flags&CANWRITE) || __fflush4(stream,0)) {
#endif

kaputt:
    stream->flags|=ERRORINDICATOR;
    return 0;
  }
  if (!nmemb || len/nmemb!=size) return 0; /* check for integer overflow */
  if (__unlikely(len>stream->buflen || (stream->flags&NOBUF))) {
#if defined USE_TM || defined USE_TMIRR
    if (tx_fflush_unlocked(stream)) return 0;
#elif defined USE_LOCK
    if (fflush_unlocked(stream)) return 0;
#else
#warning "Synchronization mechanism not set"
#endif
    do {
#if defined USE_TM || defined USE_TMIRR
      res=tx_write(stream->fd,ptr,len,&stream->sys_position);
#elif defined USE_LOCK
      res=write(stream->fd,ptr,len);
#else
#warning "Synchronization mechanism not set"
#endif
    } while (res==-1 && errno==EINTR);
  } else {
    /* try to make the common case fast */
    size_t todo=stream->buflen-stream->bm;
    if (todo>len) todo=len;

    if (todo) {
      if (stream->flags&BUFLINEWISE) {
       if (__unlikely((stream->flags&CHECKLINEWISE)!=0)) {
         stream->flags&=~CHECKLINEWISE;
         /* stdout is set to BUFLINEWISE|CHECKLINEWISE by default. */
         /* that means we should check whether it is connected to a
          * tty on first flush, and if not so, reset BUFLINEWISE */          
         if (!stream->is_terminal) {
           stream->flags&=~BUFLINEWISE;
           goto notlinewise;
         }
      }
      for (i=0; i<todo; ++i) {
         if ((stream->buf[stream->bm++]=((char*)ptr)[i])=='\n') {
#if defined USE_TM || defined USE_TMIRR
           if (tx_fflush_unlocked(stream)) goto kaputt;
#elif defined USE_LOCK
           if (fflush_unlocked(stream)) goto kaputt;
#else
#warning "Synchronization mechanism not set"
#endif
         }
       }

      } else {
notlinewise:
        dst= stream->buf+stream->bm;
        src = (char*)ptr;
        i = todo;
        while(i) {
          *(dst++) = *(src++);
          i--;
        }
        // memcpy(stream->buf+bm,ptr,todo);
        stream->bm = stream->bm+todo;
      }
      done=todo;
    } else
      done=0;
    for (i=done; i<len; ++i)
#if defined USE_TM || defined USE_TMIRR
      if (tx_fputc_unlocked(((char*)ptr)[i],stream) == EOF) {
#elif defined USE_LOCK
      if (fputc_unlocked(((char*)ptr)[i],stream) == EOF) {
#else
#warning "Synchronization mechanism not set"
#endif
       res=len-i;
       goto abort;
      }
    res=len;
  }
  if (res<0) {
    stream->flags|=ERRORINDICATOR;
    return 0;
  }
abort:
  return size?res/size:0;
}

size_t fwrite_unlocked_lock(const void *ptr, size_t size, size_t nmemb, FILE *stream) {
  ssize_t res;
  size_t len=size*nmemb;
  size_t i,done;
  if (!__likely(stream->flags&CANWRITE) || __fflush4(stream,0)) {
kaputt:
    stream->flags|=ERRORINDICATOR;
    return 0;
  }
  if (!nmemb || len/nmemb!=size) return 0; /* check for integer overflow */
  if (__unlikely(len>stream->buflen || (stream->flags&NOBUF))) {
    if (fflush_unlocked(stream)) return 0;
    do {
      res=__libc_write(stream->fd,ptr,len);
    } while (res==-1 && errno==EINTR);
  } else {
    /* try to make the common case fast */
    size_t todo=stream->buflen-stream->bm;
    if (todo>len) todo=len;

    if (todo) {
      if (stream->flags&BUFLINEWISE) {
       if (__unlikely((stream->flags&CHECKLINEWISE)!=0)) {
         stream->flags&=~CHECKLINEWISE;
         /* stdout is set to BUFLINEWISE|CHECKLINEWISE by default. */
         /* that means we should check whether it is connected to a
          * tty on first flush, and if not so, reset BUFLINEWISE */
         if (!isatty(stream->fd)) {
           stream->flags&=~BUFLINEWISE;
           goto notlinewise;
         }
       }
       for (i=0; i<todo; ++i) {
           if ((stream->buf[stream->bm++]=((char*)ptr)[i])=='\n') {
               if (fflush_unlocked(stream)) goto kaputt;
           }
       }
      } else {
notlinewise:
       memcpy(stream->buf+stream->bm,ptr,todo);
       stream->bm+=todo;
      }
      done=todo;
    } else
      done=0;
    for (i=done; i<len; ++i)
      if (fputc_unlocked(((char*)ptr)[i],stream) == EOF) {
	res=len-i;
	goto abort;
      }
    res=len;
  }
  if (res<0) {
    stream->flags|=ERRORINDICATOR;
    return 0;
  }
abort:
  return size?res/size:0;
}

//size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream) __attribute__((weak,alias("fwrite_unlocked")));
