#include <stdio.h>
#include <dietstdio.h>
#include <unistd.h>
#include <endian.h>
#include <assert.h>

int tx_fputc_unlocked(int c, FILE *stream) {
#if defined USE_TM || defined USE_TMIRR
  if (!__likely(stream->flags&CANWRITE) || tx___fflush4(stream,0)) {
#elif defined USE_LOCK
  if (!__likely(stream->flags&CANWRITE) || __fflush4(stream,0)) {
#else
#warning "Synchronization mechanism not set"
#endif

kaputt:
    stream->flags|=ERRORINDICATOR;
    return EOF;
  }
  if (__unlikely(stream->bm>=stream->buflen-1))
#if defined USE_TM || defined USE_TMIRR
    if (tx_fflush_unlocked(stream)) goto kaputt;
#elif defined USE_LOCK
    if (fflush_unlocked(stream)) goto kaputt;
#else
#warning "Synchronization mechanism not set"
#endif
  if (stream->flags&NOBUF) {
#if __BYTE_ORDER == __LITTLE_ENDIAN
#if defined USE_TM || defined USE_TMIRR
    if (tx_write(stream->fd,&c,1,&stream->sys_position) != 1)
#else
    if (__libc_write(stream->fd,&c,1) != 1)
#endif
#else
#if defined USE_TM || defined USE_TMIRR
    if (tx_write(stream->fd,(char*)&c+sizeof(c)-1,1,&stream->sys_position) != 1)
#else
    if (__libc_write(stream->fd,(char*)&c+sizeof(c)-1,1) != 1)
#endif
#endif
      goto kaputt;
    return (unsigned char)c;
  }
  stream->buf[stream->bm]=c;
  ++stream->bm;
  if (((stream->flags&BUFLINEWISE) && c=='\n') ||
      ((stream->flags&NOBUF))) /* puke */
    if (fflush_unlocked(stream)) goto kaputt;
  return (unsigned char)c;
}


int fputc_unlocked(int c, FILE *stream) {
  if (!__likely(stream->flags&CANWRITE) || __fflush4(stream,0)) {
kaputt:
    stream->flags|=ERRORINDICATOR;
    return EOF;
  }
  if (__unlikely(stream->bm>=stream->buflen-1))
    if (fflush_unlocked(stream)) goto kaputt;
  if (stream->flags&NOBUF) {
#if __BYTE_ORDER == __LITTLE_ENDIAN
    if (__libc_write(stream->fd,&c,1) != 1)
#else
    if (__libc_write(stream->fd,(char*)&c+sizeof(c)-1,1) != 1)
#endif
      goto kaputt;
    return (unsigned char)c;
  }
  stream->buf[stream->bm]=c;
  ++stream->bm;
  if (((stream->flags&BUFLINEWISE) && c=='\n') ||
      ((stream->flags&NOBUF))) /* puke */
    if (fflush_unlocked(stream)) goto kaputt;
  return (unsigned char)c;
}

//int fputc(int c,FILE* stream) __attribute__((weak,alias("fputc_unlocked")));
