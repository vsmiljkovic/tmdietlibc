#include <stdarg.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include "dietstdio.h"
#include <detrans.h>

int printf(const char *format,...)
{
  int n;
  va_list arg_ptr;
#ifdef ADHOC_SUPPORT
  int perf_event_enabled = is_perf_event_enabled();
  if (perf_event_enabled) { 
    perf_event_disable();
  }
#endif
  va_start(arg_ptr, format);
  n=vprintf(format, arg_ptr);
  va_end(arg_ptr);
#ifdef ADHOC_SUPPORT
  if (perf_event_enabled) {
    perf_event_enable();
  }
#endif
  return n;
}
