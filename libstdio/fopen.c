#include <sys/types.h>
#include <dietstdio.h>
#include <unistd.h>
#include <stdarg.h>

extern int __stdio_atexit;

int __libc_open(const char*fn,int flags,...) __attribute__ ((transaction_pure));
int __stdio_parse_mode(const char *mode) __attribute__ ((transaction_pure));

/* this is needed so the libpthread wrapper can initialize the mutex,
 * not to lock it */

FILE *fopen_unlocked(const char *path, const char *mode) {
  int f=0;	/* O_RDONLY, O_WRONLY or O_RDWR */
  int fd;
  FILE* stream;

  f=__stdio_parse_mode(mode);
  if ((fd=__libc_open(path,f,0666))<0)
    return 0;
  stream=__stdio_init_file_nothreads(fd,1,f);
  stream->sys_position = lseek(stream->fd,0,SEEK_CUR);  // tm-change: file position update 

  if (stream->is_terminal == -1) { /* TM-dietlibc */
    stream->is_terminal = isatty(stream->fd);
  }

  return stream;
}

//FILE *fopen(const char *path, const char *mode) __attribute__((weak,alias("fopen_unlocked")));

/**
 * File open function with the arguments of __libc_open. 
**/

FILE* open_unlocked(const char *path, int flags, int m) {
  int fd;
  FILE* stream;
  
  if (m != -1)
    fd=__libc_open(path,flags,m);
  else
    fd=__libc_open(path,flags);
  if (fd<0)
    return 0;

  stream=__stdio_init_file_nothreads(fd,1,flags);
  stream->sys_position = lseek(stream->fd,0,SEEK_CUR);  // tm-change: file position update 
  return stream;
}
