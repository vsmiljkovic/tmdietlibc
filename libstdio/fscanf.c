#include <stdarg.h>
#include <sys/types.h>
#include <stdlib.h>
#include "dietstdio.h"
#include <unistd.h>
#include <dietlibc-tanger-stm.h>

int fscanf(FILE *stream, const char *format, ...) {
  int n;
  va_list arg_ptr;
  va_start(arg_ptr, format);

  pthread_mutex_lock(&stream->m);
  n = vfscanf(stream,format,arg_ptr);
   pthread_mutex_unlock(&stream->m);

  va_end(arg_ptr);
  return n;
}
