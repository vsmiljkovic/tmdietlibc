#include <dietstdio.h>
#include <unistd.h>
#include <dietlibc-tanger-stm.h>

int fseek_unlocked(FILE *stream, long offset, int whence) {
#if defined USE_TM || defined USE_TMIRR 
  tx_fflush_unlocked(stream);
#elif defined USE_LOCK
  fflush_unlocked(stream);
#else
#warning "Synchronization mechanism not set"
#endif
  stream->bm=0; stream->bs=0;
  stream->flags&=~(ERRORINDICATOR|EOFINDICATOR);
  stream->ungotten=0;
#if defined USE_TM || defined USE_TMIRR 
  return tx_lseek(stream->fd,offset,whence,&stream->sys_position)!=-1?0:-1;
#elif defined USE_LOCK
  return lseek(stream->fd,offset,whence)!=-1?0:-1;
#else
#warning "Synchronization mechanism not set"
#endif
}

/**
 * The same function as fseek_unlocked, the only difference is the return value.
 * It returns new offset (not like fseek that returns 0 or -1).
**/

int seek_unlocked(FILE *stream, long offset, int whence) {
#if defined USE_TM || defined USE_TMIRR 
  tx_fflush_unlocked(stream);
#elif defined USE_LOCK
  fflush_unlocked(stream);
#else
#warning "Synchronization mechanism not set"
#endif
  stream->bm=0; stream->bs=0;
  stream->flags&=~(ERRORINDICATOR|EOFINDICATOR);
  stream->ungotten=0;
#if defined USE_TM || defined USE_TMIRR 
  return tx_lseek(stream->fd,offset,whence,&stream->sys_position);
#elif defined USE_LOCK
  return lseek(stream->fd,offset,whence);
#else
#warning "Synchronization mechanism not set"
#endif
}
