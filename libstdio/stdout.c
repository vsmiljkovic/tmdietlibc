#include <dietstdio.h>
#include <dietlibc-tanger-stm.h>

static char __stdout_buf[BUFSIZE];
static FILE __stdout = {
  .fd=1,
  .flags=BUFLINEWISE|STATICBUF|CANWRITE|CHECKLINEWISE,
  .bs=0, .bm=0,
  .buflen=BUFSIZE,
  .buf=__stdout_buf,
  .next=0,
  .popen_kludge=0,
  .ungetbuf=0,
  .ungotten=0,
//"  .was_modified=0,
  .is_terminal=-1,
#ifdef WANT_THREAD_SAFE
  .m=PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP,
#endif
};

FILE *stdout=&__stdout;

int __fflush_stdout(void) {
#if defined USE_TM || defined USE_TMIRR
  return tx_fflush_unlocked(stdout);
#elif defined USE_LOCK
  return fflush_unlocked(stdout);
#else
#warning "Synchronization mechanism not set"
#endif
}
