#include "dietstdio.h"
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

static int tanger_wrapperpure_mkstemp(char* template) __attribute__ ((weakref("mkstemp")));
static int tanger_wrapperpure_unlink(const char *pathname) __attribute__ ((weakref("unlink")));

/* this is needed so the libpthread wrapper can initialize the mutex,
 * not to lock it */

FILE *tmpfile_unlocked(void) {
  int fd;
  char template[20] = "/tmp/tmpfile-XXXXXX";
  if ((fd=mkstemp(template))<0)
    return 0;
  unlink(template);
  return __stdio_init_file_nothreads(fd,1,O_RDWR);
}

//FILE *tmpfile(void) __attribute__((weak,alias("tmpfile_unlocked")));
