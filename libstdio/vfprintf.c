#include <stdarg.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include "dietstdio.h"

int vfprintf_unlocked(FILE *stream, const char *format, va_list arg_ptr)
{
//  struct arg_printf ap = { stream, (int(*)(void*,size_t,void*)) __fwrite_unlocked };
  return __v_printf_unlocked(stream,format,arg_ptr);
}

int vfprintf_lock(FILE *stream, const char *format, va_list arg_ptr)
{
//  struct arg_printf ap = { stream, (int(*)(void*,size_t,void*)) __fwrite_unlocked };
  return __v_printf_lock(stream,format,arg_ptr);
}
