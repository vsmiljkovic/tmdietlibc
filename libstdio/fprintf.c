#include <stdarg.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include "dietstdio.h"
#include <unistd.h>
#include <dietlibc-tanger-stm.h>

int fprintf(FILE *f,const char *format,...) {
  int n;
  va_list arg_ptr;
  va_start(arg_ptr,format);

  pthread_mutex_lock(&f->m);
  n=vfprintf_unlocked(f,format,arg_ptr);
  pthread_mutex_unlock(&f->m);

  va_end(arg_ptr);
  return n;
}


int fprintf_lock(FILE *f,const char *format,...) {
  int n;
  va_list arg_ptr;
  va_start(arg_ptr,format);

  pthread_mutex_lock(&f->m);
  n=vfprintf_lock(f,format,arg_ptr);
  pthread_mutex_unlock(&f->m);
 
  va_end(arg_ptr);
  return n;
}
