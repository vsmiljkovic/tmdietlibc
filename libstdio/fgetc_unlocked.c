#include "dietstdio.h"
#include <unistd.h>

int fgetc_unlocked(FILE *stream) {
  if (__unlikely(!(stream->flags&CANREAD))) goto kaputt;
  if (__unlikely(stream->ungotten)) {
    stream->ungotten=0;
    return stream->ungetbuf;
  }

  /* common case first */
  if (__likely(stream->bm<stream->bs))
    return (unsigned char)stream->buf[stream->bm++];

  if (__unlikely(feof_unlocked(stream)))
    return EOF;
#if defined USE_TM || defined USE_TMIRR
  if (tx___fflush4(stream,BUFINPUT)) return EOF;
#elif defined USE_LOCK
  if (__fflush4(stream,BUFINPUT)) return EOF;
#else
#warning "Synchronization mechanism not set"
#endif
  if (__unlikely(stream->bm>=stream->bs)) {
#if defined USE_TM || defined USE_TMIRR
    ssize_t len=tx_read(stream->fd,stream->buf,stream->buflen, &stream->sys_position);
#elif defined USE_LOCK
    ssize_t len=read(stream->fd,stream->buf,stream->buflen);
#else
#warning "Synchronization mechanism not set"
#endif
    if (len==0) {
      stream->flags|=EOFINDICATOR;
      return EOF;
    } else if (len<0) {
kaputt:
      stream->flags|=ERRORINDICATOR;
      return EOF;
    }
    stream->bm=0;
    stream->bs=len;
  }
  return stream->buf[stream->bm++];
}
