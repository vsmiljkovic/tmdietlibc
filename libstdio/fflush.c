#include <stdlib.h>
#include "dietwarning.h"
#include "dietstdio.h"
#include <assert.h>

FILE *__stdio_root;

int __stdio_atexit=0;

void __stdio_flushall(void) {
    fflush_unlocked(0);
}

void __stdio_flushall_lock(void) {
    fflush_lock(0);
}

int fflush_unlocked(FILE *stream) {
    if (stream==0) {
        int res;
	FILE *f;
        //__fflush_stdin();
        fflush_unlocked(stdin);
        //__fflush_stdout();
        fflush_unlocked(stdout);
        //__fflush_stderr();
        fflush_unlocked(stderr);
        for (res=0, f=__stdio_root; f; f=f->next)
            if (fflush_unlocked(f))
                res=-1;
        return res;
    }

    if (stream->flags&BUFINPUT) {
        register int tmp;
        if ((tmp=stream->bm-stream->bs)) {
            lseek(stream->fd,tmp,SEEK_CUR);
        }
        stream->bs=stream->bm=0;
    } else {
      if (stream->bm && write(stream->fd,stream->buf,stream->bm)!=(ssize_t)stream->bm) {
        stream->flags|=ERRORINDICATOR;
        return -1;
      }
      stream->bm=0;
    }
    stream->ungotten=0;
    return 0;
}


/* Tx version of fflush_unlocked */

int tx_fflush_unlocked(FILE *stream) {
    if (stream==0) {
        int res;
        FILE *f;
        __fflush_stdin();
        __fflush_stdout();
        __fflush_stderr();
        for (res=0, f=__stdio_root; f; f=f->next)
            if (tx_fflush_unlocked(f))
                res=-1;
        return res;
    }
    
    if (stream->flags&BUFINPUT) {
        register int tmp;
        if ((tmp=stream->bm-stream->bs)) {

#if defined USE_TM || defined USE_TMIRR
            tx_lseek(stream->fd,tmp,SEEK_CUR, &stream->sys_position);
#elif defined USE_LOCK
            lseek(stream->fd,tmp,SEEK_CUR);
#else
#warning "Synchronization mechanism not set"
#endif
        }
        stream->bs=stream->bm=0;
    } else {
#if defined USE_TM || defined USE_TMIRR
        if (stream->bm && tx_write(stream->fd,stream->buf,stream->bm,&stream->sys_position)!=(ssize_t)stream->bm) {
#elif defined USE_LOCK
        if (stream->bm && write(stream->fd,stream->buf,stream->bm)!=(ssize_t)stream->bm) {
#else
#warning "Synchronization mechanism not set"
#endif
        stream->flags|=ERRORINDICATOR;
        return -1;
      }
      stream->bm=0;
    }
    stream->ungotten=0;
    return 0;
}

int __fflush4(FILE *stream,int next) {
  if (__unlikely(!__stdio_atexit)) {
    __stdio_atexit=1;
    atexit(__stdio_flushall);
  }
  if (__unlikely((stream->flags&BUFINPUT)!=next)) {
    int res=fflush_unlocked(stream);
    stream->flags=(stream->flags&~BUFINPUT)|next;
    return res;
  }
  if (stream->fd==0 && __stdin_is_tty()) __fflush_stdout();
  return 0;
}

/* Tx version of __fflush4 */
int tx___fflush4(FILE *stream,int next) {
    if (__unlikely(!__stdio_atexit)) {
        __stdio_atexit=1;
        atexit(__stdio_flushall);
    }

    if (__unlikely((stream->flags&BUFINPUT)!=next)) {
#if defined USE_TM || defined USE_TMIRR
        int res=tx_fflush_unlocked(stream);
#elif defined USE_LOCK
        int res=fflush_unlocked(stream);
#else
#warning "Synchronization mechanism not set"
#endif
        stream->flags=(stream->flags&~BUFINPUT)|next;
        return res;
    }
    if (stream->fd==0 && __stdin_is_tty()) __fflush_stdout();
    return 0;
}

/* Internal function, has no prototype.
 * This is defined here because of the weak symbol ELF semantics */
int __stdio_outs(const char *s,size_t len);
int __stdio_outs(const char *s,size_t len) {
    return fwrite_lock(s,1,(size_t)len,stdout)==len?1:0;
}

link_warning("fflush","warning: your code uses stdio (7+k bloat).")
