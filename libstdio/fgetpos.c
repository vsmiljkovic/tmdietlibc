#include <stdio.h>

int fgetpos_unlocked(FILE *stream, fpos_t *pos) {
  long l=ftell_unlocked(stream);
  if (l==-1) return -1;
  *pos=l;
  return 0;
}
