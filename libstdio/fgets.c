#include "dietstdio.h"


void* pure_memchr(const void *s, int c, size_t n) {
  const unsigned char *pc = (unsigned char *) s;
  for (;n--;pc++)
    if (*pc == c)
      return ((void *) pc);
  return 0;
}

void* pure_memcpy (void *dst, const void *src, size_t n)
{
    void           *res = dst;
    unsigned char  *c1, *c2;
    c1 = (unsigned char *) dst;
    c2 = (unsigned char *) src;
    while (n--) *c1++ = *c2++;
    return (res);
}

char *fgets_unlocked(char *s, int size, FILE *stream) {
  int l;
  if (size<=0) return 0;
  for (l=0; l+1<size; ) {
    register int c;
    if (l && __likely(stream->bm<stream->bs)) {
      /* try common case first */
      c=(unsigned char)stream->buf[stream->bm++];
    } else {
      c=fgetc_unlocked(stream);
      if (__unlikely(c==EOF)) {
	if (!l) return 0;
	break;
      }
    }
    s[l]=c;
    ++l;
    if (c=='\n') break;
  }
  s[l]=0;
  return s;
}
