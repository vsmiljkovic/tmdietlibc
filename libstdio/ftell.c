#include <dietstdio.h>
#include <unistd.h>
#include <errno.h>

long ftell_unlocked(FILE *stream) {
  off_t l;
#if defined USE_TM || defined USE_TMIRR 
//  TM-dietlibc change: when it's the end of a file, return the file position, instead of -1
// if (stream->flags&3 || (l=tx_lseek(stream->fd,0,SEEK_CUR, &stream->sys_position))==-1) return -1;
  if (stream->flags&1 || (l=tx_lseek(stream->fd,0,SEEK_CUR, &stream->sys_position))==-1) return -1;
#elif defined USE_LOCK
//  TM-dietlibc change: when it's the end of a file, return the file position, instead of -1
//  if (stream->flags&3 || (l=lseek(stream->fd,0,SEEK_CUR))==-1) return -1;
  if (stream->flags&1 || (l=lseek(stream->fd,0,SEEK_CUR))==-1) return -1;
#else
#warning "Synchronization mechanism not set"
#endif
  if (stream->flags&BUFINPUT)
    return l-(stream->bs-stream->bm)-stream->ungotten;
  else
    return l+stream->bm;
}

//long ftell(FILE *stream) __attribute__((weak,alias("ftell_unlocked")));
