#include <stdio.h>

int fsetpos_unlocked(FILE *stream, fpos_t *pos) {
  if (fseek_unlocked(stream,*pos,SEEK_SET)==-1)
    return -1;
  return 0;
}
