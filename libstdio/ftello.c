#include <dietstdio.h>
#include <unistd.h>

off_t ftello_unlocked(FILE *stream) {
  off_t l;
#if defined USE_TM || defined USE_TMIRR 
  if (tx_fflush_unlocked(stream)) return -1;
  return ((l=tx_lseek(stream->fd,0,SEEK_CUR,&stream->sys_position))==-1?-1:l-stream->ungotten);
#elif defined USE_LOCK
  if (fflush_unlocked(stream)) return -1;
  return ((l=lseek(stream->fd,0,SEEK_CUR))==-1?-1:l-stream->ungotten);
#else
#warning "Synchronization mechanism not set"
#endif
}

//off_t ftello(FILE *stream) __attribute__((weak,alias("ftello_unlocked")));
