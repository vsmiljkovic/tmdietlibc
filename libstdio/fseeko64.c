#include <dietstdio.h>
#include <unistd.h>
#include <sys/stat.h>

#ifndef __NO_STAT64
int fseeko64_unlocked(FILE *stream, off64_t offset, int whence) {
#if defined USE_TM || defined USE_TMIRR 
  tx_fflush_unlocked(stream);
#elif defined USE_LOCK
  fflush_unlocked(stream);
#else
#warning "Synchronization mechanism not set"
#endif
  stream->bm=0; stream->bs=0;
  stream->flags&=~(ERRORINDICATOR|EOFINDICATOR);
  stream->ungotten=0;
#if defined USE_TM || defined USE_TMIRR 
  return tx_lseek(stream->fd,offset,whence,&stream->sys_position)!=-1?0:-1;
#elif defined USE_LOCK
  return lseek64(stream->fd,offset,whence)!=-1?0:-1;
#else
#warning "Synchronization mechanism not set"
#endif
}

//int fseeko64(FILE *stream, off64_t offset, int whence) __attribute__((weak,alias("fseeko64_unlocked")));
#endif
